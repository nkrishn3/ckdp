﻿
namespace CKDPacking
{
    partial class frmStations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmStations));
            this.panel1 = new System.Windows.Forms.Panel();
            this.stgroupbox = new System.Windows.Forms.GroupBox();
            this.stlable2 = new System.Windows.Forms.Label();
            this.stpicturebox = new System.Windows.Forms.PictureBox();
            this.stlable = new System.Windows.Forms.Label();
            this.btn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.pnlstation = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.fpnl = new System.Windows.Forms.FlowLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.stgroupbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stpicturebox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.fpnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(688, 71);
            this.panel1.TabIndex = 0;
            // 
            // stgroupbox
            // 
            this.stgroupbox.Controls.Add(this.stlable2);
            this.stgroupbox.Controls.Add(this.stpicturebox);
            this.stgroupbox.Controls.Add(this.stlable);
            this.stgroupbox.Controls.Add(this.btn);
            this.stgroupbox.Location = new System.Drawing.Point(227, 3);
            this.stgroupbox.Name = "stgroupbox";
            this.stgroupbox.Size = new System.Drawing.Size(294, 227);
            this.stgroupbox.TabIndex = 6;
            this.stgroupbox.TabStop = false;
            // 
            // stlable2
            // 
            this.stlable2.AutoSize = true;
            this.stlable2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stlable2.Location = new System.Drawing.Point(29, 165);
            this.stlable2.Name = "stlable2";
            this.stlable2.Size = new System.Drawing.Size(75, 18);
            this.stlable2.TabIndex = 6;
            this.stlable2.Text = "Plan Name";
            // 
            // stpicturebox
            // 
            this.stpicturebox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.stpicturebox.Dock = System.Windows.Forms.DockStyle.Top;
            this.stpicturebox.Location = new System.Drawing.Point(3, 16);
            this.stpicturebox.Name = "stpicturebox";
            this.stpicturebox.Size = new System.Drawing.Size(288, 94);
            this.stpicturebox.TabIndex = 1;
            this.stpicturebox.TabStop = false;
            this.stpicturebox.Click += new System.EventHandler(this.stpicturebox_Click);
            // 
            // stlable
            // 
            this.stlable.AutoSize = true;
            this.stlable.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stlable.Location = new System.Drawing.Point(29, 139);
            this.stlable.Name = "stlable";
            this.stlable.Size = new System.Drawing.Size(91, 18);
            this.stlable.TabIndex = 5;
            this.stlable.Text = "Station Name";
            // 
            // btn
            // 
            this.btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn.Location = new System.Drawing.Point(3, 16);
            this.btn.Name = "btn";
            this.btn.Size = new System.Drawing.Size(288, 208);
            this.btn.TabIndex = 0;
            this.btn.Text = "btn3";
            this.btn.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(39, 23);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // pnlstation
            // 
            this.pnlstation.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlstation.BackgroundImage")));
            this.pnlstation.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlstation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlstation.Location = new System.Drawing.Point(0, 388);
            this.pnlstation.Name = "pnlstation";
            this.pnlstation.Size = new System.Drawing.Size(688, 82);
            this.pnlstation.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(218, 230);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(9, 177);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Plan Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Station Name";
            // 
            // button1
            // 
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(3, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(212, 211);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // fpnl
            // 
            this.fpnl.Controls.Add(this.groupBox1);
            this.fpnl.Controls.Add(this.stgroupbox);
            this.fpnl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fpnl.Location = new System.Drawing.Point(0, 71);
            this.fpnl.Name = "fpnl";
            this.fpnl.Size = new System.Drawing.Size(688, 317);
            this.fpnl.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(3, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(171, 120);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // frmStations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(688, 470);
            this.Controls.Add(this.fpnl);
            this.Controls.Add(this.pnlstation);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmStations";
            this.Text = "Stations";
            this.Load += new System.EventHandler(this.frmStations_Load);
            this.panel1.ResumeLayout(false);
            this.stgroupbox.ResumeLayout(false);
            this.stgroupbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.stpicturebox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.fpnl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlstation;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FlowLayoutPanel fpnl;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox stgroupbox;
        private System.Windows.Forms.Label stlable2;
        private System.Windows.Forms.PictureBox stpicturebox;
        private System.Windows.Forms.Label stlable;
        private System.Windows.Forms.Button btn;
    }
}