﻿
namespace CKDPacking
{
    partial class frmProdcutionHold
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbProdcution = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.BtnEdit = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtFGMANTRK = new System.Windows.Forms.TextBox();
            this.lblFRAGMANTR = new System.Windows.Forms.Label();
            this.txtSo = new System.Windows.Forms.TextBox();
            this.lblSaleOrder = new System.Windows.Forms.Label();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.txtFrame = new System.Windows.Forms.TextBox();
            this.txtPull = new System.Windows.Forms.TextBox();
            this.dtpTodate = new System.Windows.Forms.DateTimePicker();
            this.lblToDate = new System.Windows.Forms.Label();
            this.cmbPlant = new System.Windows.Forms.ComboBox();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblFrameBox = new System.Windows.Forms.Label();
            this.lblPullNo = new System.Windows.Forms.Label();
            this.lblPlant = new System.Windows.Forms.Label();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.dgvProdPlan = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbProdcution.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdPlan)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel1.Controls.Add(this.gbProdcution);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(708, 287);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgvProdPlan);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 287);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(708, 217);
            this.panel2.TabIndex = 1;
            // 
            // gbProdcution
            // 
            this.gbProdcution.AutoSize = true;
            this.gbProdcution.BackColor = System.Drawing.Color.Lavender;
            this.gbProdcution.Controls.Add(this.panel3);
            this.gbProdcution.Controls.Add(this.txtFGMANTRK);
            this.gbProdcution.Controls.Add(this.lblFRAGMANTR);
            this.gbProdcution.Controls.Add(this.txtSo);
            this.gbProdcution.Controls.Add(this.lblSaleOrder);
            this.gbProdcution.Controls.Add(this.cmbStatus);
            this.gbProdcution.Controls.Add(this.txtFrame);
            this.gbProdcution.Controls.Add(this.txtPull);
            this.gbProdcution.Controls.Add(this.dtpTodate);
            this.gbProdcution.Controls.Add(this.lblToDate);
            this.gbProdcution.Controls.Add(this.cmbPlant);
            this.gbProdcution.Controls.Add(this.dtpFromDate);
            this.gbProdcution.Controls.Add(this.lblStatus);
            this.gbProdcution.Controls.Add(this.lblFrameBox);
            this.gbProdcution.Controls.Add(this.lblPullNo);
            this.gbProdcution.Controls.Add(this.lblPlant);
            this.gbProdcution.Controls.Add(this.lblFromDate);
            this.gbProdcution.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbProdcution.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbProdcution.Location = new System.Drawing.Point(0, 0);
            this.gbProdcution.Name = "gbProdcution";
            this.gbProdcution.Size = new System.Drawing.Size(708, 287);
            this.gbProdcution.TabIndex = 1;
            this.gbProdcution.TabStop = false;
            this.gbProdcution.Text = "Production Plan";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnClear);
            this.panel3.Controls.Add(this.BtnEdit);
            this.panel3.Controls.Add(this.btnSearch);
            this.panel3.Location = new System.Drawing.Point(159, 237);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(250, 36);
            this.panel3.TabIndex = 25;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClear.Font = new System.Drawing.Font("Cambria", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(177, 1);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(71, 35);
            this.btnClear.TabIndex = 23;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            // 
            // BtnEdit
            // 
            this.BtnEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.BtnEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BtnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.BtnEdit.Font = new System.Drawing.Font("Cambria", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnEdit.Location = new System.Drawing.Point(88, 0);
            this.BtnEdit.Name = "BtnEdit";
            this.BtnEdit.Size = new System.Drawing.Size(71, 36);
            this.BtnEdit.TabIndex = 22;
            this.BtnEdit.Text = "Edit";
            this.BtnEdit.UseVisualStyleBackColor = false;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSearch.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSearch.Font = new System.Drawing.Font("Cambria", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(0, 0);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(71, 36);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtFGMANTRK
            // 
            this.txtFGMANTRK.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFGMANTRK.Location = new System.Drawing.Point(402, 136);
            this.txtFGMANTRK.Name = "txtFGMANTRK";
            this.txtFGMANTRK.Size = new System.Drawing.Size(160, 26);
            this.txtFGMANTRK.TabIndex = 19;
            // 
            // lblFRAGMANTR
            // 
            this.lblFRAGMANTR.AutoSize = true;
            this.lblFRAGMANTR.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFRAGMANTR.Location = new System.Drawing.Point(305, 141);
            this.lblFRAGMANTR.Name = "lblFRAGMANTR";
            this.lblFRAGMANTR.Size = new System.Drawing.Size(73, 15);
            this.lblFRAGMANTR.TabIndex = 18;
            this.lblFRAGMANTR.Tag = ":";
            this.lblFRAGMANTR.Text = "FGMANTRK:";
            // 
            // txtSo
            // 
            this.txtSo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSo.Location = new System.Drawing.Point(402, 88);
            this.txtSo.Name = "txtSo";
            this.txtSo.Size = new System.Drawing.Size(160, 26);
            this.txtSo.TabIndex = 17;
            // 
            // lblSaleOrder
            // 
            this.lblSaleOrder.AutoSize = true;
            this.lblSaleOrder.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaleOrder.Location = new System.Drawing.Point(305, 93);
            this.lblSaleOrder.Name = "lblSaleOrder";
            this.lblSaleOrder.Size = new System.Drawing.Size(69, 15);
            this.lblSaleOrder.TabIndex = 16;
            this.lblSaleOrder.Text = "Sale Order:";
            // 
            // cmbStatus
            // 
            this.cmbStatus.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(402, 183);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(160, 26);
            this.cmbStatus.TabIndex = 15;
            // 
            // txtFrame
            // 
            this.txtFrame.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrame.Location = new System.Drawing.Point(109, 183);
            this.txtFrame.Name = "txtFrame";
            this.txtFrame.Size = new System.Drawing.Size(150, 26);
            this.txtFrame.TabIndex = 14;
            // 
            // txtPull
            // 
            this.txtPull.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPull.Location = new System.Drawing.Point(109, 136);
            this.txtPull.Name = "txtPull";
            this.txtPull.Size = new System.Drawing.Size(150, 26);
            this.txtPull.TabIndex = 13;
            // 
            // dtpTodate
            // 
            this.dtpTodate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTodate.Location = new System.Drawing.Point(402, 44);
            this.dtpTodate.Name = "dtpTodate";
            this.dtpTodate.Size = new System.Drawing.Size(160, 26);
            this.dtpTodate.TabIndex = 9;
            this.dtpTodate.Value = new System.DateTime(2021, 4, 20, 0, 0, 0, 0);
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            this.lblToDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToDate.Location = new System.Drawing.Point(305, 55);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(52, 15);
            this.lblToDate.TabIndex = 8;
            this.lblToDate.Text = "To Date:";
            // 
            // cmbPlant
            // 
            this.cmbPlant.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPlant.FormattingEnabled = true;
            this.cmbPlant.Location = new System.Drawing.Point(109, 82);
            this.cmbPlant.Name = "cmbPlant";
            this.cmbPlant.Size = new System.Drawing.Size(150, 26);
            this.cmbPlant.TabIndex = 7;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(109, 41);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(150, 26);
            this.dtpFromDate.TabIndex = 6;
            this.dtpFromDate.Value = new System.DateTime(2021, 4, 20, 0, 0, 0, 0);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(305, 194);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(45, 15);
            this.lblStatus.TabIndex = 5;
            this.lblStatus.Text = "Status:";
            // 
            // lblFrameBox
            // 
            this.lblFrameBox.AutoSize = true;
            this.lblFrameBox.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrameBox.Location = new System.Drawing.Point(7, 194);
            this.lblFrameBox.Name = "lblFrameBox";
            this.lblFrameBox.Size = new System.Drawing.Size(88, 15);
            this.lblFrameBox.TabIndex = 4;
            this.lblFrameBox.Text = "Frame Box No:";
            // 
            // lblPullNo
            // 
            this.lblPullNo.AutoSize = true;
            this.lblPullNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPullNo.Location = new System.Drawing.Point(6, 147);
            this.lblPullNo.Name = "lblPullNo";
            this.lblPullNo.Size = new System.Drawing.Size(57, 15);
            this.lblPullNo.TabIndex = 3;
            this.lblPullNo.Text = "PULL No:";
            // 
            // lblPlant
            // 
            this.lblPlant.AutoSize = true;
            this.lblPlant.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlant.Location = new System.Drawing.Point(7, 93);
            this.lblPlant.Name = "lblPlant";
            this.lblPlant.Size = new System.Drawing.Size(75, 15);
            this.lblPlant.TabIndex = 1;
            this.lblPlant.Text = "Plant Name:";
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromDate.Location = new System.Drawing.Point(7, 52);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(69, 15);
            this.lblFromDate.TabIndex = 0;
            this.lblFromDate.Text = "From Date:";
            // 
            // dgvProdPlan
            // 
            this.dgvProdPlan.AllowUserToAddRows = false;
            this.dgvProdPlan.AllowUserToDeleteRows = false;
            this.dgvProdPlan.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvProdPlan.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProdPlan.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProdPlan.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvProdPlan.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgvProdPlan.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvProdPlan.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvProdPlan.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvProdPlan.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProdPlan.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvProdPlan.ColumnHeadersHeight = 30;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.InactiveBorder;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProdPlan.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvProdPlan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProdPlan.EnableHeadersVisualStyles = false;
            this.dgvProdPlan.Location = new System.Drawing.Point(0, 0);
            this.dgvProdPlan.MultiSelect = false;
            this.dgvProdPlan.Name = "dgvProdPlan";
            this.dgvProdPlan.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvProdPlan.RowHeadersVisible = false;
            this.dgvProdPlan.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.InactiveBorder;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvProdPlan.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvProdPlan.RowTemplate.Height = 40;
            this.dgvProdPlan.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProdPlan.Size = new System.Drawing.Size(708, 217);
            this.dgvProdPlan.TabIndex = 2;
            // 
            // frmProdcutionHold
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 504);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmProdcutionHold";
            this.Text = "Manage Production Hold";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.gbProdcution.ResumeLayout(false);
            this.gbProdcution.PerformLayout();
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProdPlan)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox gbProdcution;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button BtnEdit;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.TextBox txtFGMANTRK;
        private System.Windows.Forms.Label lblFRAGMANTR;
        private System.Windows.Forms.TextBox txtSo;
        private System.Windows.Forms.Label lblSaleOrder;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.TextBox txtFrame;
        private System.Windows.Forms.TextBox txtPull;
        private System.Windows.Forms.DateTimePicker dtpTodate;
        private System.Windows.Forms.Label lblToDate;
        private System.Windows.Forms.ComboBox cmbPlant;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblFrameBox;
        private System.Windows.Forms.Label lblPullNo;
        private System.Windows.Forms.Label lblPlant;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.DataGridView dgvProdPlan;
    }
}