﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CKDPacking
{
    public partial class FrmUser : Form
   {
        string create_emp_id;
        string create_user_type;
        public FrmUser(string empid, string usertype)
        {
            InitializeComponent();
            create_emp_id = empid;
            create_user_type = usertype;
        }

        private void User_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.Fixed3D;

            this.WindowState = FormWindowState.Maximized;
        }
    }
}
