﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKDPacking
{
    public static class Global
    {

        public static string LoginUserID;
        public static string UserRole;
        public static string ApplicationVersion;
        public static string AssemblyLine;
        public static string Plant;
        public static string PortName;
        public static string TimeOut;
        public static string PastMonthVinGeneration;
        public static string PLCSentData;
        public static bool isPortOpened;
        public static string PLCSentDataMTO;

        public static void LoadDefaultValues()
        {
            SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());

            con.Open();

            SqlCommand cmd = new SqlCommand("SELECT KeyName,KeyValue FROM dbo.Configuration", con);
            cmd.CommandType = CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();
            Hashtable htConfiguration = new Hashtable();

            while (dr.Read())
            {
                htConfiguration.Add(dr["KeyName"], dr["KeyValue"]);
            }

            ApplicationVersion = Convert.ToString(htConfiguration["ApplicationVersion"]);
            Plant = Convert.ToString(htConfiguration["Plant"]);
            PortName = Convert.ToString(htConfiguration["PortName"]);
            TimeOut = Convert.ToString(htConfiguration["TimeOut"]);
            PastMonthVinGeneration = Convert.ToString(htConfiguration["PastMonthVinGeneration"]);


            con.Close();
        }

    }
}
