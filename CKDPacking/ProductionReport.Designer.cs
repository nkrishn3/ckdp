﻿
namespace CKDPacking
{
    partial class frmProductReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.gbProdcution = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtPrimary = new System.Windows.Forms.TextBox();
            this.lblprimaryboxNo = new System.Windows.Forms.Label();
            this.txtFGMANTRK = new System.Windows.Forms.TextBox();
            this.lblFRAGMANTR = new System.Windows.Forms.Label();
            this.txtSo = new System.Windows.Forms.TextBox();
            this.lblSaleOrder = new System.Windows.Forms.Label();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.txtFrame = new System.Windows.Forms.TextBox();
            this.txtPull = new System.Windows.Forms.TextBox();
            this.txtHu = new System.Windows.Forms.TextBox();
            this.cmbStation = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dtpTodate = new System.Windows.Forms.DateTimePicker();
            this.lblToDate = new System.Windows.Forms.Label();
            this.cmbPlant = new System.Windows.Forms.ComboBox();
            this.dtpFromDate = new System.Windows.Forms.DateTimePicker();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblFrameBox = new System.Windows.Forms.Label();
            this.lblPullNo = new System.Windows.Forms.Label();
            this.lblHuNo = new System.Windows.Forms.Label();
            this.lblPlant = new System.Windows.Forms.Label();
            this.lblFromDate = new System.Windows.Forms.Label();
            this.pnlDGV = new System.Windows.Forms.Panel();
            this.dgvProduction = new System.Windows.Forms.DataGridView();
            this.pnlSearch.SuspendLayout();
            this.gbProdcution.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlDGV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduction)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pnlSearch.Controls.Add(this.gbProdcution);
            this.pnlSearch.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSearch.Location = new System.Drawing.Point(0, 0);
            this.pnlSearch.Name = "pnlSearch";
            this.pnlSearch.Size = new System.Drawing.Size(712, 393);
            this.pnlSearch.TabIndex = 0;
            // 
            // gbProdcution
            // 
            this.gbProdcution.AutoSize = true;
            this.gbProdcution.BackColor = System.Drawing.Color.Lavender;
            this.gbProdcution.Controls.Add(this.panel1);
            this.gbProdcution.Controls.Add(this.txtPrimary);
            this.gbProdcution.Controls.Add(this.lblprimaryboxNo);
            this.gbProdcution.Controls.Add(this.txtFGMANTRK);
            this.gbProdcution.Controls.Add(this.lblFRAGMANTR);
            this.gbProdcution.Controls.Add(this.txtSo);
            this.gbProdcution.Controls.Add(this.lblSaleOrder);
            this.gbProdcution.Controls.Add(this.cmbStatus);
            this.gbProdcution.Controls.Add(this.txtFrame);
            this.gbProdcution.Controls.Add(this.txtPull);
            this.gbProdcution.Controls.Add(this.txtHu);
            this.gbProdcution.Controls.Add(this.cmbStation);
            this.gbProdcution.Controls.Add(this.label8);
            this.gbProdcution.Controls.Add(this.dtpTodate);
            this.gbProdcution.Controls.Add(this.lblToDate);
            this.gbProdcution.Controls.Add(this.cmbPlant);
            this.gbProdcution.Controls.Add(this.dtpFromDate);
            this.gbProdcution.Controls.Add(this.lblStatus);
            this.gbProdcution.Controls.Add(this.lblFrameBox);
            this.gbProdcution.Controls.Add(this.lblPullNo);
            this.gbProdcution.Controls.Add(this.lblHuNo);
            this.gbProdcution.Controls.Add(this.lblPlant);
            this.gbProdcution.Controls.Add(this.lblFromDate);
            this.gbProdcution.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbProdcution.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbProdcution.Location = new System.Drawing.Point(0, 0);
            this.gbProdcution.Name = "gbProdcution";
            this.gbProdcution.Size = new System.Drawing.Size(712, 393);
            this.gbProdcution.TabIndex = 0;
            this.gbProdcution.TabStop = false;
            this.gbProdcution.Text = "Production Report";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnClear);
            this.panel1.Controls.Add(this.btnExport);
            this.panel1.Controls.Add(this.btnSearch);
            this.panel1.Location = new System.Drawing.Point(157, 318);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(250, 36);
            this.panel1.TabIndex = 25;
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClear.Font = new System.Drawing.Font("Cambria", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(177, 1);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(71, 35);
            this.btnClear.TabIndex = 23;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnExport
            // 
            this.btnExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnExport.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExport.Font = new System.Drawing.Font("Cambria", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExport.Location = new System.Drawing.Point(88, 0);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(71, 36);
            this.btnExport.TabIndex = 22;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = false;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnSearch.Dock = System.Windows.Forms.DockStyle.Left;
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSearch.Font = new System.Drawing.Font("Cambria", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Location = new System.Drawing.Point(0, 0);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(71, 36);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtPrimary
            // 
            this.txtPrimary.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPrimary.Location = new System.Drawing.Point(402, 223);
            this.txtPrimary.Name = "txtPrimary";
            this.txtPrimary.Size = new System.Drawing.Size(160, 26);
            this.txtPrimary.TabIndex = 21;
            // 
            // lblprimaryboxNo
            // 
            this.lblprimaryboxNo.AutoSize = true;
            this.lblprimaryboxNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblprimaryboxNo.Location = new System.Drawing.Point(300, 228);
            this.lblprimaryboxNo.Name = "lblprimaryboxNo";
            this.lblprimaryboxNo.Size = new System.Drawing.Size(96, 15);
            this.lblprimaryboxNo.TabIndex = 20;
            this.lblprimaryboxNo.Text = "Primary Box No:";
            // 
            // txtFGMANTRK
            // 
            this.txtFGMANTRK.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFGMANTRK.Location = new System.Drawing.Point(402, 174);
            this.txtFGMANTRK.Name = "txtFGMANTRK";
            this.txtFGMANTRK.Size = new System.Drawing.Size(160, 26);
            this.txtFGMANTRK.TabIndex = 19;
            // 
            // lblFRAGMANTR
            // 
            this.lblFRAGMANTR.AutoSize = true;
            this.lblFRAGMANTR.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFRAGMANTR.Location = new System.Drawing.Point(305, 179);
            this.lblFRAGMANTR.Name = "lblFRAGMANTR";
            this.lblFRAGMANTR.Size = new System.Drawing.Size(69, 15);
            this.lblFRAGMANTR.TabIndex = 18;
            this.lblFRAGMANTR.Tag = ":";
            this.lblFRAGMANTR.Text = "FGMANTRK";
            // 
            // txtSo
            // 
            this.txtSo.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSo.Location = new System.Drawing.Point(402, 131);
            this.txtSo.Name = "txtSo";
            this.txtSo.Size = new System.Drawing.Size(160, 26);
            this.txtSo.TabIndex = 17;
            // 
            // lblSaleOrder
            // 
            this.lblSaleOrder.AutoSize = true;
            this.lblSaleOrder.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSaleOrder.Location = new System.Drawing.Point(305, 136);
            this.lblSaleOrder.Name = "lblSaleOrder";
            this.lblSaleOrder.Size = new System.Drawing.Size(69, 15);
            this.lblSaleOrder.TabIndex = 16;
            this.lblSaleOrder.Text = "Sale Order:";
            // 
            // cmbStatus
            // 
            this.cmbStatus.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(97, 266);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(150, 26);
            this.cmbStatus.TabIndex = 15;
            // 
            // txtFrame
            // 
            this.txtFrame.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrame.Location = new System.Drawing.Point(97, 223);
            this.txtFrame.Name = "txtFrame";
            this.txtFrame.Size = new System.Drawing.Size(150, 26);
            this.txtFrame.TabIndex = 14;
            // 
            // txtPull
            // 
            this.txtPull.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPull.Location = new System.Drawing.Point(97, 174);
            this.txtPull.Name = "txtPull";
            this.txtPull.Size = new System.Drawing.Size(150, 26);
            this.txtPull.TabIndex = 13;
            this.txtPull.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txtHu
            // 
            this.txtHu.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHu.Location = new System.Drawing.Point(97, 131);
            this.txtHu.Name = "txtHu";
            this.txtHu.Size = new System.Drawing.Size(150, 26);
            this.txtHu.TabIndex = 12;
            // 
            // cmbStation
            // 
            this.cmbStation.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStation.FormattingEnabled = true;
            this.cmbStation.Location = new System.Drawing.Point(402, 88);
            this.cmbStation.Name = "cmbStation";
            this.cmbStation.Size = new System.Drawing.Size(160, 26);
            this.cmbStation.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(305, 93);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 15);
            this.label8.TabIndex = 10;
            this.label8.Text = "Station:";
            // 
            // dtpTodate
            // 
            this.dtpTodate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpTodate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTodate.Location = new System.Drawing.Point(402, 44);
            this.dtpTodate.Name = "dtpTodate";
            this.dtpTodate.Size = new System.Drawing.Size(160, 26);
            this.dtpTodate.TabIndex = 9;
            this.dtpTodate.Value = new System.DateTime(2021, 4, 20, 0, 0, 0, 0);
            // 
            // lblToDate
            // 
            this.lblToDate.AutoSize = true;
            this.lblToDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblToDate.Location = new System.Drawing.Point(305, 55);
            this.lblToDate.Name = "lblToDate";
            this.lblToDate.Size = new System.Drawing.Size(52, 15);
            this.lblToDate.TabIndex = 8;
            this.lblToDate.Text = "To Date:";
            // 
            // cmbPlant
            // 
            this.cmbPlant.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPlant.FormattingEnabled = true;
            this.cmbPlant.Location = new System.Drawing.Point(97, 88);
            this.cmbPlant.Name = "cmbPlant";
            this.cmbPlant.Size = new System.Drawing.Size(150, 26);
            this.cmbPlant.TabIndex = 7;
            // 
            // dtpFromDate
            // 
            this.dtpFromDate.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpFromDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromDate.Location = new System.Drawing.Point(97, 41);
            this.dtpFromDate.Name = "dtpFromDate";
            this.dtpFromDate.Size = new System.Drawing.Size(150, 26);
            this.dtpFromDate.TabIndex = 6;
            this.dtpFromDate.Value = new System.DateTime(2021, 4, 20, 0, 0, 0, 0);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus.Location = new System.Drawing.Point(6, 266);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(45, 15);
            this.lblStatus.TabIndex = 5;
            this.lblStatus.Text = "Status:";
            // 
            // lblFrameBox
            // 
            this.lblFrameBox.AutoSize = true;
            this.lblFrameBox.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFrameBox.Location = new System.Drawing.Point(3, 228);
            this.lblFrameBox.Name = "lblFrameBox";
            this.lblFrameBox.Size = new System.Drawing.Size(88, 15);
            this.lblFrameBox.TabIndex = 4;
            this.lblFrameBox.Text = "Frame Box No:";
            // 
            // lblPullNo
            // 
            this.lblPullNo.AutoSize = true;
            this.lblPullNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPullNo.Location = new System.Drawing.Point(6, 179);
            this.lblPullNo.Name = "lblPullNo";
            this.lblPullNo.Size = new System.Drawing.Size(57, 15);
            this.lblPullNo.TabIndex = 3;
            this.lblPullNo.Text = "PULL No:";
            this.lblPullNo.Click += new System.EventHandler(this.label4_Click);
            // 
            // lblHuNo
            // 
            this.lblHuNo.AutoSize = true;
            this.lblHuNo.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHuNo.Location = new System.Drawing.Point(7, 136);
            this.lblHuNo.Name = "lblHuNo";
            this.lblHuNo.Size = new System.Drawing.Size(46, 15);
            this.lblHuNo.TabIndex = 2;
            this.lblHuNo.Text = "HU No:";
            // 
            // lblPlant
            // 
            this.lblPlant.AutoSize = true;
            this.lblPlant.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlant.Location = new System.Drawing.Point(7, 93);
            this.lblPlant.Name = "lblPlant";
            this.lblPlant.Size = new System.Drawing.Size(75, 15);
            this.lblPlant.TabIndex = 1;
            this.lblPlant.Text = "Plant Name:";
            // 
            // lblFromDate
            // 
            this.lblFromDate.AutoSize = true;
            this.lblFromDate.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFromDate.Location = new System.Drawing.Point(7, 52);
            this.lblFromDate.Name = "lblFromDate";
            this.lblFromDate.Size = new System.Drawing.Size(69, 15);
            this.lblFromDate.TabIndex = 0;
            this.lblFromDate.Text = "From Date:";
            // 
            // pnlDGV
            // 
            this.pnlDGV.AutoScroll = true;
            this.pnlDGV.Controls.Add(this.dgvProduction);
            this.pnlDGV.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlDGV.Location = new System.Drawing.Point(0, 393);
            this.pnlDGV.Name = "pnlDGV";
            this.pnlDGV.Size = new System.Drawing.Size(712, 229);
            this.pnlDGV.TabIndex = 1;
            // 
            // dgvProduction
            // 
            this.dgvProduction.AllowUserToAddRows = false;
            this.dgvProduction.AllowUserToDeleteRows = false;
            this.dgvProduction.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvProduction.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvProduction.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvProduction.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvProduction.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgvProduction.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvProduction.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgvProduction.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            this.dgvProduction.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvProduction.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvProduction.ColumnHeadersHeight = 30;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.InactiveBorder;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProduction.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvProduction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvProduction.EnableHeadersVisualStyles = false;
            this.dgvProduction.Location = new System.Drawing.Point(0, 0);
            this.dgvProduction.MultiSelect = false;
            this.dgvProduction.Name = "dgvProduction";
            this.dgvProduction.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dgvProduction.RowHeadersVisible = false;
            this.dgvProduction.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.InactiveBorder;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvProduction.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvProduction.RowTemplate.Height = 40;
            this.dgvProduction.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProduction.Size = new System.Drawing.Size(712, 229);
            this.dgvProduction.TabIndex = 2;
            // 
            // frmProductReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 620);
            this.Controls.Add(this.pnlDGV);
            this.Controls.Add(this.pnlSearch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmProductReport";
            this.Text = "Production Report";
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearch.PerformLayout();
            this.gbProdcution.ResumeLayout(false);
            this.gbProdcution.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.pnlDGV.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvProduction)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.Panel pnlDGV;
        private System.Windows.Forms.GroupBox gbProdcution;
        private System.Windows.Forms.TextBox txtSo;
        private System.Windows.Forms.Label lblSaleOrder;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.TextBox txtFrame;
        private System.Windows.Forms.TextBox txtPull;
        private System.Windows.Forms.TextBox txtHu;
        private System.Windows.Forms.ComboBox cmbStation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker dtpTodate;
        private System.Windows.Forms.Label lblToDate;
        private System.Windows.Forms.ComboBox cmbPlant;
        private System.Windows.Forms.DateTimePicker dtpFromDate;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblFrameBox;
        private System.Windows.Forms.Label lblPullNo;
        private System.Windows.Forms.Label lblHuNo;
        private System.Windows.Forms.Label lblPlant;
        private System.Windows.Forms.Label lblFromDate;
        private System.Windows.Forms.TextBox txtPrimary;
        private System.Windows.Forms.Label lblprimaryboxNo;
        private System.Windows.Forms.TextBox txtFGMANTRK;
        private System.Windows.Forms.Label lblFRAGMANTR;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.DataGridView dgvProduction;
    }
}