﻿
namespace CKDPacking
{
    partial class FrmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmHome));
            this.PnlLeft = new System.Windows.Forms.Panel();
            this.PnlSetting = new System.Windows.Forms.Panel();
            this.BtnPlants = new System.Windows.Forms.Button();
            this.BtnStations = new System.Windows.Forms.Button();
            this.BtnRoles = new System.Windows.Forms.Button();
            this.BtnSettings = new System.Windows.Forms.Button();
            this.PnlPacking = new System.Windows.Forms.Panel();
            this.BtnStation = new System.Windows.Forms.Button();
            this.BtnReport = new System.Windows.Forms.Button();
            this.BtnPackingPlan = new System.Windows.Forms.Button();
            this.BtnPacking = new System.Windows.Forms.Button();
            this.PnlUser = new System.Windows.Forms.Panel();
            this.BtnAssignStation = new System.Windows.Forms.Button();
            this.BtnAddUser = new System.Windows.Forms.Button();
            this.BtnUsers = new System.Windows.Forms.Button();
            this.Pnllogo = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PnlRightHeader = new System.Windows.Forms.Panel();
            this.pnlLogout = new System.Windows.Forms.Panel();
            this.BtnChangePwd = new System.Windows.Forms.Button();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.BtnUser = new System.Windows.Forms.Button();
            this.lblUserName = new System.Windows.Forms.Label();
            this.BtnHome = new System.Windows.Forms.Button();
            this.pnlRight = new System.Windows.Forms.Panel();
            this.PnlChildForm = new System.Windows.Forms.Panel();
            this.btnSyncSAP = new System.Windows.Forms.Button();
            this.PnlLeft.SuspendLayout();
            this.PnlSetting.SuspendLayout();
            this.PnlPacking.SuspendLayout();
            this.PnlUser.SuspendLayout();
            this.Pnllogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.PnlRightHeader.SuspendLayout();
            this.pnlLogout.SuspendLayout();
            this.pnlRight.SuspendLayout();
            this.SuspendLayout();
            // 
            // PnlLeft
            // 
            this.PnlLeft.AutoScroll = true;
            this.PnlLeft.BackColor = System.Drawing.Color.Gray;
            this.PnlLeft.Controls.Add(this.btnSyncSAP);
            this.PnlLeft.Controls.Add(this.PnlSetting);
            this.PnlLeft.Controls.Add(this.BtnSettings);
            this.PnlLeft.Controls.Add(this.PnlPacking);
            this.PnlLeft.Controls.Add(this.BtnPacking);
            this.PnlLeft.Controls.Add(this.PnlUser);
            this.PnlLeft.Controls.Add(this.BtnUsers);
            this.PnlLeft.Controls.Add(this.Pnllogo);
            this.PnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.PnlLeft.Location = new System.Drawing.Point(0, 0);
            this.PnlLeft.Name = "PnlLeft";
            this.PnlLeft.Size = new System.Drawing.Size(204, 606);
            this.PnlLeft.TabIndex = 0;
            this.PnlLeft.Paint += new System.Windows.Forms.PaintEventHandler(this.PnlLeft_Paint);
            // 
            // PnlSetting
            // 
            this.PnlSetting.BackColor = System.Drawing.Color.Gray;
            this.PnlSetting.Controls.Add(this.BtnPlants);
            this.PnlSetting.Controls.Add(this.BtnStations);
            this.PnlSetting.Controls.Add(this.BtnRoles);
            this.PnlSetting.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlSetting.Location = new System.Drawing.Point(0, 411);
            this.PnlSetting.Name = "PnlSetting";
            this.PnlSetting.Size = new System.Drawing.Size(204, 105);
            this.PnlSetting.TabIndex = 6;
            // 
            // BtnPlants
            // 
            this.BtnPlants.BackColor = System.Drawing.Color.LightGray;
            this.BtnPlants.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnPlants.FlatAppearance.BorderSize = 0;
            this.BtnPlants.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnPlants.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPlants.ForeColor = System.Drawing.Color.Black;
            this.BtnPlants.Location = new System.Drawing.Point(0, 72);
            this.BtnPlants.Name = "BtnPlants";
            this.BtnPlants.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.BtnPlants.Size = new System.Drawing.Size(204, 36);
            this.BtnPlants.TabIndex = 4;
            this.BtnPlants.Text = "Plants";
            this.BtnPlants.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnPlants.UseVisualStyleBackColor = false;
            this.BtnPlants.Click += new System.EventHandler(this.BtnPlants_Click);
            // 
            // BtnStations
            // 
            this.BtnStations.BackColor = System.Drawing.Color.LightGray;
            this.BtnStations.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnStations.FlatAppearance.BorderSize = 0;
            this.BtnStations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnStations.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnStations.ForeColor = System.Drawing.Color.Black;
            this.BtnStations.Location = new System.Drawing.Point(0, 36);
            this.BtnStations.Name = "BtnStations";
            this.BtnStations.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.BtnStations.Size = new System.Drawing.Size(204, 36);
            this.BtnStations.TabIndex = 3;
            this.BtnStations.Text = "Stations";
            this.BtnStations.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnStations.UseVisualStyleBackColor = false;
            this.BtnStations.Click += new System.EventHandler(this.BtnStations_Click);
            // 
            // BtnRoles
            // 
            this.BtnRoles.BackColor = System.Drawing.Color.LightGray;
            this.BtnRoles.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnRoles.FlatAppearance.BorderSize = 0;
            this.BtnRoles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnRoles.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRoles.ForeColor = System.Drawing.Color.Black;
            this.BtnRoles.Location = new System.Drawing.Point(0, 0);
            this.BtnRoles.Name = "BtnRoles";
            this.BtnRoles.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.BtnRoles.Size = new System.Drawing.Size(204, 36);
            this.BtnRoles.TabIndex = 2;
            this.BtnRoles.Text = "Roles";
            this.BtnRoles.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnRoles.UseVisualStyleBackColor = false;
            this.BtnRoles.Click += new System.EventHandler(this.BtnRoles_Click);
            // 
            // BtnSettings
            // 
            this.BtnSettings.BackColor = System.Drawing.Color.LightGray;
            this.BtnSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnSettings.FlatAppearance.BorderSize = 0;
            this.BtnSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnSettings.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSettings.Location = new System.Drawing.Point(0, 375);
            this.BtnSettings.Name = "BtnSettings";
            this.BtnSettings.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.BtnSettings.Size = new System.Drawing.Size(204, 36);
            this.BtnSettings.TabIndex = 5;
            this.BtnSettings.Text = "Setting";
            this.BtnSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnSettings.UseVisualStyleBackColor = false;
            this.BtnSettings.Click += new System.EventHandler(this.BtnSettings_Click);
            // 
            // PnlPacking
            // 
            this.PnlPacking.BackColor = System.Drawing.Color.Gray;
            this.PnlPacking.Controls.Add(this.BtnStation);
            this.PnlPacking.Controls.Add(this.BtnReport);
            this.PnlPacking.Controls.Add(this.BtnPackingPlan);
            this.PnlPacking.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlPacking.Location = new System.Drawing.Point(0, 272);
            this.PnlPacking.Name = "PnlPacking";
            this.PnlPacking.Size = new System.Drawing.Size(204, 103);
            this.PnlPacking.TabIndex = 4;
            // 
            // BtnStation
            // 
            this.BtnStation.BackColor = System.Drawing.Color.LightGray;
            this.BtnStation.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnStation.FlatAppearance.BorderSize = 0;
            this.BtnStation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnStation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnStation.ForeColor = System.Drawing.Color.Black;
            this.BtnStation.Location = new System.Drawing.Point(0, 72);
            this.BtnStation.Name = "BtnStation";
            this.BtnStation.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.BtnStation.Size = new System.Drawing.Size(204, 36);
            this.BtnStation.TabIndex = 4;
            this.BtnStation.Text = "Stations";
            this.BtnStation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnStation.UseVisualStyleBackColor = false;
            this.BtnStation.Click += new System.EventHandler(this.BtnStation_Click);
            // 
            // BtnReport
            // 
            this.BtnReport.BackColor = System.Drawing.Color.LightGray;
            this.BtnReport.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnReport.FlatAppearance.BorderSize = 0;
            this.BtnReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnReport.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnReport.ForeColor = System.Drawing.Color.Black;
            this.BtnReport.Location = new System.Drawing.Point(0, 36);
            this.BtnReport.Name = "BtnReport";
            this.BtnReport.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.BtnReport.Size = new System.Drawing.Size(204, 36);
            this.BtnReport.TabIndex = 3;
            this.BtnReport.Text = "Report";
            this.BtnReport.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnReport.UseVisualStyleBackColor = false;
            this.BtnReport.Click += new System.EventHandler(this.BtnReport_Click);
            // 
            // BtnPackingPlan
            // 
            this.BtnPackingPlan.BackColor = System.Drawing.Color.LightGray;
            this.BtnPackingPlan.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnPackingPlan.FlatAppearance.BorderSize = 0;
            this.BtnPackingPlan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnPackingPlan.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPackingPlan.ForeColor = System.Drawing.Color.Black;
            this.BtnPackingPlan.Location = new System.Drawing.Point(0, 0);
            this.BtnPackingPlan.Name = "BtnPackingPlan";
            this.BtnPackingPlan.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.BtnPackingPlan.Size = new System.Drawing.Size(204, 36);
            this.BtnPackingPlan.TabIndex = 2;
            this.BtnPackingPlan.Text = "Packing Plan";
            this.BtnPackingPlan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnPackingPlan.UseVisualStyleBackColor = false;
            this.BtnPackingPlan.Click += new System.EventHandler(this.BtnPackingPlan_Click);
            // 
            // BtnPacking
            // 
            this.BtnPacking.BackColor = System.Drawing.Color.LightGray;
            this.BtnPacking.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnPacking.FlatAppearance.BorderSize = 0;
            this.BtnPacking.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnPacking.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnPacking.Location = new System.Drawing.Point(0, 236);
            this.BtnPacking.Name = "BtnPacking";
            this.BtnPacking.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.BtnPacking.Size = new System.Drawing.Size(204, 36);
            this.BtnPacking.TabIndex = 3;
            this.BtnPacking.Text = "Packing";
            this.BtnPacking.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnPacking.UseVisualStyleBackColor = false;
            this.BtnPacking.Click += new System.EventHandler(this.BtnPacking_Click);
            // 
            // PnlUser
            // 
            this.PnlUser.BackColor = System.Drawing.Color.Gray;
            this.PnlUser.Controls.Add(this.BtnAssignStation);
            this.PnlUser.Controls.Add(this.BtnAddUser);
            this.PnlUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlUser.Location = new System.Drawing.Point(0, 162);
            this.PnlUser.Name = "PnlUser";
            this.PnlUser.Size = new System.Drawing.Size(204, 74);
            this.PnlUser.TabIndex = 2;
            // 
            // BtnAssignStation
            // 
            this.BtnAssignStation.BackColor = System.Drawing.Color.LightGray;
            this.BtnAssignStation.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnAssignStation.FlatAppearance.BorderSize = 0;
            this.BtnAssignStation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAssignStation.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAssignStation.ForeColor = System.Drawing.Color.Black;
            this.BtnAssignStation.Location = new System.Drawing.Point(0, 36);
            this.BtnAssignStation.Name = "BtnAssignStation";
            this.BtnAssignStation.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.BtnAssignStation.Size = new System.Drawing.Size(204, 38);
            this.BtnAssignStation.TabIndex = 3;
            this.BtnAssignStation.Text = "Assign Station";
            this.BtnAssignStation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnAssignStation.UseVisualStyleBackColor = false;
            this.BtnAssignStation.Click += new System.EventHandler(this.BtnAssignUser_Click);
            // 
            // BtnAddUser
            // 
            this.BtnAddUser.BackColor = System.Drawing.Color.LightGray;
            this.BtnAddUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnAddUser.FlatAppearance.BorderSize = 0;
            this.BtnAddUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAddUser.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAddUser.ForeColor = System.Drawing.Color.Black;
            this.BtnAddUser.Location = new System.Drawing.Point(0, 0);
            this.BtnAddUser.Name = "BtnAddUser";
            this.BtnAddUser.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.BtnAddUser.Size = new System.Drawing.Size(204, 36);
            this.BtnAddUser.TabIndex = 2;
            this.BtnAddUser.Text = "Add User";
            this.BtnAddUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnAddUser.UseVisualStyleBackColor = false;
            this.BtnAddUser.Click += new System.EventHandler(this.BtnAddUser_Click);
            // 
            // BtnUsers
            // 
            this.BtnUsers.BackColor = System.Drawing.Color.LightGray;
            this.BtnUsers.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnUsers.FlatAppearance.BorderSize = 0;
            this.BtnUsers.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnUsers.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUsers.Image = ((System.Drawing.Image)(resources.GetObject("BtnUsers.Image")));
            this.BtnUsers.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.BtnUsers.Location = new System.Drawing.Point(0, 126);
            this.BtnUsers.Name = "BtnUsers";
            this.BtnUsers.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.BtnUsers.Size = new System.Drawing.Size(204, 36);
            this.BtnUsers.TabIndex = 1;
            this.BtnUsers.Text = "Users";
            this.BtnUsers.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtnUsers.UseVisualStyleBackColor = false;
            this.BtnUsers.Click += new System.EventHandler(this.BtnUsers_Click);
            // 
            // Pnllogo
            // 
            this.Pnllogo.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Pnllogo.Controls.Add(this.pictureBox1);
            this.Pnllogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.Pnllogo.Location = new System.Drawing.Point(0, 0);
            this.Pnllogo.Name = "Pnllogo";
            this.Pnllogo.Size = new System.Drawing.Size(204, 126);
            this.Pnllogo.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(27, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(155, 105);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // PnlRightHeader
            // 
            this.PnlRightHeader.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.PnlRightHeader.Controls.Add(this.pnlLogout);
            this.PnlRightHeader.Controls.Add(this.BtnUser);
            this.PnlRightHeader.Controls.Add(this.lblUserName);
            this.PnlRightHeader.Controls.Add(this.BtnHome);
            this.PnlRightHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlRightHeader.Location = new System.Drawing.Point(0, 0);
            this.PnlRightHeader.Name = "PnlRightHeader";
            this.PnlRightHeader.Size = new System.Drawing.Size(695, 64);
            this.PnlRightHeader.TabIndex = 1;
            this.PnlRightHeader.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pnlLogout
            // 
            this.pnlLogout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlLogout.BackColor = System.Drawing.Color.White;
            this.pnlLogout.Controls.Add(this.BtnChangePwd);
            this.pnlLogout.Controls.Add(this.btnLogOut);
            this.pnlLogout.Location = new System.Drawing.Point(452, 9);
            this.pnlLogout.Name = "pnlLogout";
            this.pnlLogout.Size = new System.Drawing.Size(144, 52);
            this.pnlLogout.TabIndex = 2;
            // 
            // BtnChangePwd
            // 
            this.BtnChangePwd.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.BtnChangePwd.Dock = System.Windows.Forms.DockStyle.Top;
            this.BtnChangePwd.FlatAppearance.BorderSize = 0;
            this.BtnChangePwd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnChangePwd.Font = new System.Drawing.Font("Calibri", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnChangePwd.ForeColor = System.Drawing.Color.White;
            this.BtnChangePwd.Location = new System.Drawing.Point(0, 26);
            this.BtnChangePwd.Name = "BtnChangePwd";
            this.BtnChangePwd.Size = new System.Drawing.Size(144, 27);
            this.BtnChangePwd.TabIndex = 2;
            this.BtnChangePwd.Text = "Change Password";
            this.BtnChangePwd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnChangePwd.UseVisualStyleBackColor = false;
            this.BtnChangePwd.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnLogOut
            // 
            this.btnLogOut.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.btnLogOut.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnLogOut.FlatAppearance.BorderSize = 0;
            this.btnLogOut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogOut.Font = new System.Drawing.Font("Calibri", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOut.ForeColor = System.Drawing.Color.White;
            this.btnLogOut.Location = new System.Drawing.Point(0, 0);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(144, 26);
            this.btnLogOut.TabIndex = 4;
            this.btnLogOut.Text = "Logout";
            this.btnLogOut.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLogOut.UseVisualStyleBackColor = false;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click_1);
            // 
            // BtnUser
            // 
            this.BtnUser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnUser.BackColor = System.Drawing.Color.Transparent;
            this.BtnUser.FlatAppearance.BorderSize = 0;
            this.BtnUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnUser.Font = new System.Drawing.Font("Calibri", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUser.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BtnUser.Location = new System.Drawing.Point(530, 32);
            this.BtnUser.Name = "BtnUser";
            this.BtnUser.Size = new System.Drawing.Size(165, 32);
            this.BtnUser.TabIndex = 3;
            this.BtnUser.Text = "button1";
            this.BtnUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtnUser.UseVisualStyleBackColor = false;
            this.BtnUser.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblUserName
            // 
            this.lblUserName.AutoSize = true;
            this.lblUserName.BackColor = System.Drawing.Color.Transparent;
            this.lblUserName.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserName.ForeColor = System.Drawing.Color.White;
            this.lblUserName.Location = new System.Drawing.Point(6, 28);
            this.lblUserName.Name = "lblUserName";
            this.lblUserName.Size = new System.Drawing.Size(80, 33);
            this.lblUserName.TabIndex = 2;
            this.lblUserName.Text = "Home";
            this.lblUserName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblUserName.Click += new System.EventHandler(this.lblUserName_Click);
            // 
            // BtnHome
            // 
            this.BtnHome.Location = new System.Drawing.Point(6, 3);
            this.BtnHome.Name = "BtnHome";
            this.BtnHome.Size = new System.Drawing.Size(75, 23);
            this.BtnHome.TabIndex = 0;
            this.BtnHome.Text = "Home";
            this.BtnHome.UseVisualStyleBackColor = true;
            this.BtnHome.Click += new System.EventHandler(this.BtnHome_Click);
            // 
            // pnlRight
            // 
            this.pnlRight.Controls.Add(this.PnlChildForm);
            this.pnlRight.Controls.Add(this.PnlRightHeader);
            this.pnlRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRight.Location = new System.Drawing.Point(204, 0);
            this.pnlRight.Name = "pnlRight";
            this.pnlRight.Size = new System.Drawing.Size(695, 606);
            this.pnlRight.TabIndex = 3;
            // 
            // PnlChildForm
            // 
            this.PnlChildForm.AutoScroll = true;
            this.PnlChildForm.BackColor = System.Drawing.Color.Lavender;
            this.PnlChildForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PnlChildForm.Location = new System.Drawing.Point(0, 64);
            this.PnlChildForm.Name = "PnlChildForm";
            this.PnlChildForm.Size = new System.Drawing.Size(695, 542);
            this.PnlChildForm.TabIndex = 4;
            // 
            // btnSyncSAP
            // 
            this.btnSyncSAP.BackColor = System.Drawing.Color.LightGray;
            this.btnSyncSAP.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSyncSAP.FlatAppearance.BorderSize = 0;
            this.btnSyncSAP.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSyncSAP.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSyncSAP.Location = new System.Drawing.Point(0, 516);
            this.btnSyncSAP.Name = "btnSyncSAP";
            this.btnSyncSAP.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.btnSyncSAP.Size = new System.Drawing.Size(204, 36);
            this.btnSyncSAP.TabIndex = 7;
            this.btnSyncSAP.Text = "Sync Prodcution Plan";
            this.btnSyncSAP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSyncSAP.UseVisualStyleBackColor = false;
            // 
            // FrmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(899, 606);
            this.Controls.Add(this.pnlRight);
            this.Controls.Add(this.PnlLeft);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FrmHome";
            this.Text = "CKD Packing";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmHome_FormClosing);
            this.Load += new System.EventHandler(this.Home_Load);
            this.PnlLeft.ResumeLayout(false);
            this.PnlSetting.ResumeLayout(false);
            this.PnlPacking.ResumeLayout(false);
            this.PnlUser.ResumeLayout(false);
            this.Pnllogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.PnlRightHeader.ResumeLayout(false);
            this.PnlRightHeader.PerformLayout();
            this.pnlLogout.ResumeLayout(false);
            this.pnlRight.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel PnlLeft;
        private System.Windows.Forms.Panel PnlUser;
        private System.Windows.Forms.Button BtnAssignStation;
        private System.Windows.Forms.Button BtnAddUser;
        private System.Windows.Forms.Button BtnUsers;
        private System.Windows.Forms.Button BtnPacking;
        private System.Windows.Forms.Panel PnlPacking;
        private System.Windows.Forms.Button BtnStation;
        private System.Windows.Forms.Button BtnReport;
        private System.Windows.Forms.Button BtnPackingPlan;
        private System.Windows.Forms.Panel PnlSetting;
        private System.Windows.Forms.Button BtnPlants;
        private System.Windows.Forms.Button BtnStations;
        private System.Windows.Forms.Button BtnRoles;
        private System.Windows.Forms.Button BtnSettings;
        private System.Windows.Forms.Panel Pnllogo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel PnlRightHeader;
        private System.Windows.Forms.Button BtnHome;
        private System.Windows.Forms.Button BtnUser;
        private System.Windows.Forms.Label lblUserName;
        private System.Windows.Forms.Panel pnlLogout;
        private System.Windows.Forms.Button BtnChangePwd;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.Panel pnlRight;
        private System.Windows.Forms.Panel PnlChildForm;
        private System.Windows.Forms.Button btnSyncSAP;
    }
}