﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Linq;
using System.Text;   
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;



namespace CKDPacking
{
    public partial class FrmLogin : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public string Plant;
        DialogResult resultmsg;
        public FrmLogin()
        {
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
           
          
            string ApplicationVersion = ConfigurationManager.AppSettings["ApplicationVersion"];
            string Environment = ConfigurationManager.AppSettings["Environment"];

            Plant = getPlantName();
            string AssemblyLine = ConfigurationManager.AppSettings["AssemblyLine"];
            
            if (ValidateData())
            {
                //  if (ValidateLoginIP())
                // {

                if (ChkRememberMe.Checked)
                {
                    Properties.Settings.Default.UserName = TxtUserName.Text;
                    Properties.Settings.Default.Password = TxtPassword.Text;
                    Properties.Settings.Default.Save();
                }

                if (ChkRememberMe.Checked == false)
                {
                    Properties.Settings.Default.UserName = string.Empty;
                    Properties.Settings.Default.Password = string.Empty;
                    Properties.Settings.Default.Save();
                }
                try

                {
                    SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());

                    con.Open();

                    SqlCommand cmd = new SqlCommand(ApplicationConstants.LOGIN_SELECT_QUERY, con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@UserName", TxtUserName.Text);
                    cmd.Parameters.AddWithValue("@Password", TxtPassword.Text);


                    SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    adapt.Fill(ds);
                    con.Close();
                    int count = ds.Tables[0].Rows.Count;
                    // Global.LoadDefaultValues();
                     //If count is equal to 1, than show Main Page
                    if (count >= 1)
                    {                                      
                           string employeeCode = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                            string userRole = ds.Tables[0].Rows[0].ItemArray[1].ToString();
                            Global.LoginUserID = employeeCode;
                            Global.UserRole = userRole;
                            Global.Plant = Plant;
                            Global.AssemblyLine = AssemblyLine;
                            this.Hide();
                          
                                
                                FrmHome fhome = new FrmHome(employeeCode, userRole);
                                fhome.Show();
                                             


                                                       // fm.Text = "FactRE Connect (" + Environment + ")" + " - " + ApplicationVersion + " - " + Plant + " - " + AssemblyLine;
                            //fm.Show();
                            log.Info("Logged in as " + TxtUserName.Text);
                        
                     }
                    else
                    {
                              MessageBox.Show($"User Name and Password Mismatch",
                              "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        log.Error("User Name and Password Mismatch" + TxtUserName.Text);

                    }
                }
                catch (Exception ex)
                {
                    log.Debug("Issue in Sql Server" + ex.Message);
                    log.Error(ex.Message);
                    MessageBox.Show("Some issue in Sql Server:" + ex.Message);
                }


                // }

            }
            else
            {
                MessageBox.Show($"Please correct validation errors:\n {erpLoginPage.GetError(TxtUserName)} \n { erpLoginPage.GetError(TxtPassword)}",
                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void frmlogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private void frmlogin_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UserName != string.Empty)
            {
                TxtUserName.Text = Properties.Settings.Default.UserName;
                TxtPassword.Text = Properties.Settings.Default.Password;
            }

            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        private bool ValidateData()
        {
            erpLoginPage.Clear();
            bool Isvalid = true;

            if (string.IsNullOrEmpty(TxtUserName.Text.Trim()))
            {
                TxtUserName.Focus();
                erpLoginPage.SetError(TxtUserName, "Please Enter UserName");
                Isvalid = false;
            }

            if (string.IsNullOrEmpty(TxtPassword.Text.Trim()))
            {
                TxtPassword.Focus();
                erpLoginPage.SetError(TxtPassword, "Please Enter Password");
                Isvalid = false;
            }


            return Isvalid;
        }
        private void txtUserName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar) || Char.IsLetter(e.KeyChar) || (e.KeyChar == '\b') || e.KeyChar == 13 || e.KeyChar == 47)) //Make sure the entered key is a number (0-9)
            {

                e.Handled = true;

                MessageBox.Show($"Enter a valid Username",
                                 "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void frmLogin_Load(object sender, EventArgs e)
        {
            if (Properties.Settings.Default.UserName != string.Empty)

            {
                TxtUserName.Text = Properties.Settings.Default.UserName;
                TxtPassword.Text = Properties.Settings.Default.Password;
                ChkRememberMe.Checked = true;

            }

            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
        }
        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
       {
            if (e.KeyCode == Keys.Down)
            {
                BtnLogIn.PerformClick();
            }

        }
        private string getPlantName()
        {
            string PlantName = string.Empty;
            try
            {
                string PlantCode = ConfigurationManager.AppSettings["Plant"];
                SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());

                con.Open();

                SqlCommand cmd = new SqlCommand(ApplicationConstants.GET_PLANT_QUERY, con);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@PlantKey", PlantCode);
                PlantName = Convert.ToString(cmd.ExecuteScalar());
                con.Close();

            }
            catch (SqlException ex)
            {
                log.Error(ex.Message);
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                MessageBox.Show(ex.Message);
            }
            return PlantName;
        }
      
        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void TxtUserName_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
