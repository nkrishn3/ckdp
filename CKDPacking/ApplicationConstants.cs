﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CKDPacking
{
    class ApplicationConstants
    {   
        //user Procedure
        public static string LOGIN_SELECT_QUERY = "ckd_ValidateLogin";
        public static string USER_SELECT_QUERY = "ckd_GetUsers";
        public static string GET_PLANT_QUERY = "ckd_GetPlantName";
        public static string GET_ROLES = "ckd_GetRoles";
        public static string USER_INSERT = "ckd_InsertUser";
        public static string USER_UPDATE = "ckd_UpdateUser";
        public static string USER_DELETE = "ckd_DeleteUser";
        public static string GET_PLANTS = "ckd_GetPlants";
        public static string GET_STATIONS = "ckd_GetStations";
        public static string GET_USERLOV = "ckd_GetUserLov";
        public static string GET_USERSTATIONS = "ckd_GetUserStations";
        public static string INSERT_USERSTATIONS = "ckd_InsertUserStation";
        public static string UPDATE_USERSTATIONS = "ckd_UpdateUserStations";
        public static string DELETE_USERSTATIONS = "ckd_DeleteUserStation";

        //Product
        public static string GET_PRODUCTREPORT = "ckd_GetProductReport";



    }
}
