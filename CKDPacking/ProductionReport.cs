﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using ClosedXML.Excel;
using System.Configuration;
using System.IO;


namespace CKDPacking
{
    public partial class frmProductReport : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public frmProductReport()
        {
            InitializeComponent();
            dtpFromDate.Value = Convert.ToDateTime("01/01/1990");
            dtpTodate.Value = Convert.ToDateTime("01/01/1990");
            FillCombobox();
            BindGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
        }
        protected void FillCombobox()

        {
            DataRow drPlant;
            DataRow drStation;
            //DataRow drUser;
            try
            {
                SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                con.Open();
                SqlCommand cmd = new SqlCommand(ApplicationConstants.GET_PLANTS, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                drPlant = dt.NewRow();
                //dr.ItemArray = new object[] { 0, "--Select Role--" };
                dt.Rows.InsertAt(drPlant, 0);
                cmbPlant.ValueMember = "plantId";
                cmbPlant.DisplayMember = "plantName";
                cmbPlant.DataSource = dt;
                con.Close();
            }
            catch (SqlException ex)
            {
                log.Error("SqlException while getting plants:FillCombobox()-" + ex.Message);
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                log.Error("Exception while getting plants:FillCombobox()-" + ex.Message);
                MessageBox.Show(ex.Message);
            }

            try
            {
                SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                con.Open();
                SqlCommand cmd = new SqlCommand(ApplicationConstants.GET_STATIONS, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                drStation = dt.NewRow();
                //dr.ItemArray = new object[] { 0, "--Select Role--" };
                dt.Rows.InsertAt(drStation, 0);
                cmbStation.ValueMember = "stationId";
                cmbStation.DisplayMember = "stationNumber";
                cmbStation.DataSource = dt;
                con.Close();
            }
            catch (SqlException ex)
            {
                log.Error("SqlException While Getting Stations:FillCombobox()-" + ex.Message);
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                log.Error("Exception While Getting Stations:FillCombobox()-" + ex.Message);
                MessageBox.Show(ex.Message);
            }


            cmbStatus.Items.Add("Completed");
            cmbStatus.Items.Add("In Work");



        }
        private void BindGrid(string frmDate, string todate, string Plant, string station, string HuNo, string SoNo, string PullNo, string FGMANTR,string framboxno,string primaryboxno,string status)
        {
            //string tdy_dte = dte;
            //string to_Dte = t_Date;
            try
            {
                SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                SqlCommand cmd = new SqlCommand(ApplicationConstants.GET_PRODUCTREPORT, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@FromDate", frmDate);
                cmd.Parameters.AddWithValue("@ToDate", todate);
                cmd.Parameters.AddWithValue("@PlantId", Plant);
                cmd.Parameters.AddWithValue("@StationId", station);
                cmd.Parameters.AddWithValue("@HuNo", HuNo);
                cmd.Parameters.AddWithValue("@SoNo", SoNo);
                cmd.Parameters.AddWithValue("@PullNo", PullNo);
                cmd.Parameters.AddWithValue("@FGMATNR", FGMANTR);
                cmd.Parameters.AddWithValue("@FrameBox", framboxno);
                cmd.Parameters.AddWithValue("@PrimaryBox", primaryboxno);
                cmd.Parameters.AddWithValue("@Status", status);
                log.Info("BIND---------------");
                log.Info(
                    "\ndtpFromDate.Value.ToString" + frmDate + "\ndtpTodate.Value" + todate + "\ncmbPlant.SelectedValue.ToString()" + Plant + "\ncmbStation.SelectedValue.ToString()" + station+ "\ntxtHu.Text" + HuNo+ "\ntxtSo.Text" + SoNo+ "\ntxtPull.Text" + PullNo + "\n txtFGMANTRK.Text" +FGMANTR + "\ntxtFrame.Text" + framboxno + "\ntxtPrimary.Text" + primaryboxno + "\ncmbStatus.Text" + status);

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                
                dgvProduction.DataSource = dt;
                // DgvUser.Columns[0].Visible = false;
                //DgvUser.Columns[2].Visible = false;
                //DgvUser.Columns[09].DefaultCellStyle.Format = ApplicationConstants.DateFormat;
                //DgvUser.Columns[11].DefaultCellStyle.Format = ApplicationConstants.DateFormat;
                dgvProduction.EditMode = DataGridViewEditMode.EditProgrammatically;
                con.Close();
            }
            catch (SqlException ex)
            {
                log.Info("SqlException while prodction Report" + ex.Message);
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                log.Info("Exception while prodction Report" + ex.Message);
                MessageBox.Show(ex.Message);
            }

        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {
            
        }
        private void dtpFromDate_DropDown(object sender, EventArgs e)
        {
            dtpFromDate.Format = DateTimePickerFormat.Short;
            dtpFromDate.Value = DateTime.Today;
        }

        private void dtpToDate_DropDown(object sender, EventArgs e)
        {
            dtpTodate.Format = DateTimePickerFormat.Short;
            dtpTodate.Value = DateTime.Today;
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            cmbPlant.Text = "";
            cmbStation.Text = "";
            txtHu.Text = "";
            txtSo.Text="";
            txtPull.Text = "";
            txtFGMANTRK.Text = "";
            txtFrame.Text = "";
            txtPrimary.Text = "";
            cmbStatus.Text = "";
            dtpFromDate.Value = Convert.ToDateTime("01/01/1990");
            dtpTodate.Value = Convert.ToDateTime("01/01/1990");
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid(dtpFromDate.Value.ToString("MM/dd/yyyy"), dtpTodate.Value.ToString("MM/dd/yyyy"), cmbPlant.SelectedValue.ToString(), cmbStation.SelectedValue.ToString(), txtHu.Text, txtSo.Text, txtPull.Text, txtFGMANTRK.Text, txtFrame.Text, txtPrimary.Text, cmbStatus.Text);
            log.Info("Search---------------");
            log.Info(
                "\ndtpFromDate.Value.ToString" + dtpFromDate.Value.ToString("MM/dd/yyyy")+"\ndtpTodate.Value" + dtpTodate.Value.ToString("MM/dd/yyyy") + "\ncmbPlant.SelectedValue.ToString()" + cmbPlant.SelectedValue.ToString() + "\ncmbStation.SelectedValue.ToString()" + cmbStation.SelectedValue.ToString()+"\ntxtHu.Text" + txtHu.Text + "\ntxtSo.Text" + txtSo.Text + "\ntxtPull.Text" + txtPull.Text + "\n txtFGMANTRK.Text"+ txtFGMANTRK.Text+"\ntxtFrame.Text"+ txtFrame.Text+"\ntxtPrimary.Text"+ txtPrimary.Text+ "\ncmbStatus.Text"+ cmbStatus.Text);
        }


        private void btnExport_Click(object sender, EventArgs e)
        {
            log.Warn("Excel Export");

            DataTable dt = new DataTable();

            //Adding the Columns
            foreach (DataGridViewColumn column in dgvProduction.Columns)
            {
                if (column.DisplayIndex != dgvProduction.Columns.Count)
                {
                    dt.Columns.Add(column.HeaderText, column.ValueType);
                }
            }

            //Adding the Rows
            foreach (DataGridViewRow row in dgvProduction.Rows)
            {
                dt.Rows.Add();
                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.ColumnIndex >= 0)
                    {
                        dt.Rows[dt.Rows.Count - 1][cell.ColumnIndex] = cell.Value.ToString();
                    }
                }
            }

            //Exporting to Excel
             string folderPath = ConfigurationManager.AppSettings["DownloadPath"];
            //string folderPath= "C:\\Excel\\";
            string fileName = "Production Report_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";

            if (!System.IO.Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt, "ProductionReport");

                wb.SaveAs(folderPath + fileName);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;
            }

            System.Diagnostics.Process.Start(folderPath + fileName);
        }
    
    }
}
