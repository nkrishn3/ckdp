﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CKDPacking
{
    public partial class StationUser : Form
    {
        public StationUser()
        {
            InitializeComponent();
            station();
           // pictureBox1.BackgroundImage = "C:\Users\\pradeepG\\Desktop\\Royal Enfield\\CKD\\Development\\CKDPacking\\Resource\\error\\station.png";
        }
        private void station()
        {

            SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
            SqlCommand cmd;
            con.Open();
            cmd = new SqlCommand("ckd_GetStations", con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                string roleName = Convert.ToString(dr["stationNumber"]);
                createButton(roleName);


            }
            con.Close();
        }
        private void createButton(string name)
        {
            
            
            
            
            

            //gbox.SuspendLayout();
            //SuspendLayout();


            // gbox
            // 
            GroupBox gbox = new GroupBox();

            flowLayoutPanel1.Controls.Add(gbox);

            gbox.Location = new System.Drawing.Point(3, 3);
            gbox.Name = "gbox"+ name;
            gbox.Size = new System.Drawing.Size(209, 202);
            gbox.TabIndex = 0;
            gbox.TabStop = false;
            // 
            // btn1
            //
            Button btn1 = new Button();
            gbox.Controls.Add(btn1);
            btn1.Dock = System.Windows.Forms.DockStyle.Fill;
            btn1.Location = new System.Drawing.Point(3, 16);

            btn1.Image = global::CKDPacking.Properties.Resources.Plant1;
            btn1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            btn1.Name = "btn1"+ name;
            btn1.Size = new System.Drawing.Size(203, 183);
            btn1.TabIndex = 0;
            btn1.Text = "btn1"+ name;
            btn1.UseVisualStyleBackColor = true;
            // 
            // lbl1
            // 
            Label lbl1 = new Label();
            gbox.Controls.Add(lbl1);
            lbl1.AutoSize = true;
            lbl1.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lbl1.Location = new System.Drawing.Point(27, 148);
            lbl1.Name = "lbl1"+ name;
            lbl1.Size = new System.Drawing.Size(45, 15);
            lbl1.TabIndex = 1;
            lbl1.Text = "Station"+ name;
            lbl1.BringToFront();
            // 
            // lbl2
            //
            Label lbl2 = new Label();
            gbox.Controls.Add(lbl2);
            lbl2.AutoSize = true;
            lbl2.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lbl2.Location = new System.Drawing.Point(27, 172);
            lbl2.Name = "lbl2"+ name;
            lbl2.Size = new System.Drawing.Size(36, 15);
            lbl2.TabIndex = 2;
            lbl2.Text = "Plant"+ name;
            lbl2.BringToFront();

           /* PictureBox pb1 = new PictureBox();
            gbox.Controls.Add(pb1);
            pb1.BackgroundImage = global::CKDPacking.Properties.Resources.station;
            pb1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            pb1.Dock = System.Windows.Forms.DockStyle.Top;
            pb1.Location = new System.Drawing.Point(3, 16);
            pb1.Name = "pictureBox1";
            pb1.Size = new System.Drawing.Size(203, 129);
            pb1.TabIndex = 3;
            pb1.TabStop = false;
           */
        }
    
    }
}
