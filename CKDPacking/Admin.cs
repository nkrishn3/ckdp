﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CKDPacking
{
    public partial class FrmAdmin : Form
     {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        string create_emp_id;
        string create_user_type;
        public FrmAdmin(string empid, string usertype)
        {
             InitializeComponent();
            create_emp_id = empid;
            create_user_type = usertype;
            BindGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
        }
        private void BindGrid(string EmployeeId, string EmployeeName, string EmployeeUserName, string ContractorName, string EmployeeRole)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                SqlCommand cmd = new SqlCommand(ApplicationConstants.USER_SELECT_QUERY, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmployeeId", EmployeeId);
                cmd.Parameters.AddWithValue("@EmployeeName", EmployeeName);
                cmd.Parameters.AddWithValue("@EmployeeUserName", EmployeeUserName);
                cmd.Parameters.AddWithValue("@ContractorName", ContractorName);
                cmd.Parameters.AddWithValue("@EmployeeRole", EmployeeRole);
                cmd.Parameters.AddWithValue("@UserId", 0);

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                DgvUser.DataSource = dt;
                DgvUser.Columns[0].Visible = false;
                DgvUser.Columns[2].Visible = false;
                //DgvUser.Columns[09].DefaultCellStyle.Format = ApplicationConstants.DateFormat;
                //DgvUser.Columns[11].DefaultCellStyle.Format = ApplicationConstants.DateFormat;
                DgvUser.EditMode = DataGridViewEditMode.EditProgrammatically;
                con.Close();
            }
            catch(Exception ex)
            {
                log.Error(ex.Message);
                MessageBox.Show(ex.Message);
            }



        }
        private void Admin_Load(object sender, EventArgs e)
        {
            this.FormBorderStyle = FormBorderStyle.Fixed3D;

            this.WindowState = FormWindowState.Maximized;
            DgvUser.ColumnHeadersDefaultCellStyle.Font = new Font("Arial", 12, FontStyle.Bold);
        } 

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void BtnSearchUser_Click(object sender, EventArgs e)
        {
             BindGrid(TxtEmployeeID.Text,textBox2.Text,textBox3.Text,textBox4.Text,comboBox1.Text);
        }
    }
}
