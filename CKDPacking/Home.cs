﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CKDPacking
{
    public partial class FrmHome : Form 
    {
        string create_emp_id;
        string create_user_type;
        public FrmHome(string empid, string usertype)
        {
            InitializeComponent();
            create_emp_id = empid;
            create_user_type = usertype;
            CustomizeDesign();
            //lblUserName.Text = "Welcome "+create_emp_id;
            
           BtnUser.Text = "Welcome " + create_emp_id;
            pnlLogout.Visible = false;
            if (create_user_type == "Admin")
            {
                OpenChildForm(new AddUser(Global.LoginUserID, Global.UserRole));
            }


        }

        private void Home_Load(object sender, EventArgs e)
        {
           
            

                   
        }
        private void CustomizeDesign()
        {
            PnlUser.Visible = false;
            PnlPacking.Visible = false;
            PnlSetting.Visible = false;
        }
        private void HideSubMenu()
        {
            if (PnlUser.Visible == true)
                PnlUser.Visible = false;
            if (PnlPacking.Visible == true)
                PnlPacking.Visible = false;
            if (PnlSetting.Visible == true)
                PnlSetting.Visible = false;
        }
        private void ShowSubMenu(Panel SubMenu)
        {
            if(SubMenu.Visible==false)
            {
                HideSubMenu();
                SubMenu.Visible = true;
            }
            else
            {
                SubMenu.Visible = false;
            }
          
        }
        private Form ActiveForm = null;

        private void OpenChildForm(Form ChildForm)
        { 
            if(ActiveForm!=null)
              ActiveForm.Close();
                ActiveForm = ChildForm;
                ChildForm.TopLevel = false;
                ChildForm.FormBorderStyle = FormBorderStyle.None;
                ChildForm.Dock = DockStyle.Fill;
                PnlChildForm.Controls.Add(ChildForm);
                PnlChildForm.Tag = ChildForm;
                ChildForm.BringToFront();
                ChildForm.Show();
            lblUserName.Text = ChildForm.Text;
                       
        }
        
        

        private void PnlLeft_Paint(object sender, PaintEventArgs e)
        {

        }
        #region UserMenu
        private void BtnUsers_Click(object sender, EventArgs e)
        {
            ShowSubMenu(PnlUser);
        }
        private void BtnAddUser_Click(object sender, EventArgs e)
        {
            OpenChildForm(new AddUser(Global.LoginUserID, Global.UserRole));
            HideSubMenu();
        }
        private void BtnAssignUser_Click(object sender, EventArgs e)
        {
            OpenChildForm(new frmAssignRole());
            HideSubMenu();
        }
        private void BtnChangePassword_Click(object sender, EventArgs e)
        {

            HideSubMenu();

        }
        #endregion UserMenu

        #region Packing
        private void BtnPacking_Click(object sender, EventArgs e)
        {
            ShowSubMenu(PnlPacking);
        }
        private void BtnPackingPlan_Click(object sender, EventArgs e)
        {
            OpenChildForm(new frmProdcutionHold());
            HideSubMenu();

        }

        private void BtnReport_Click(object sender, EventArgs e)
        {
            OpenChildForm(new frmProductReport());
            HideSubMenu();
           
        }

        private void BtnStation_Click(object sender, EventArgs e)
        {
           // OpenChildForm(new frmStations());
            OpenChildForm(new StationUser());
            HideSubMenu();
        }

        private void BtnSyncSap_Click(object sender, EventArgs e)
        {
            OpenChildForm(new StationUser());
            HideSubMenu();
        }
        #endregion Packing
        #region Setting
        private void BtnSettings_Click(object sender, EventArgs e)
        {
            ShowSubMenu(PnlSetting);
        }
        private void BtnRoles_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void BtnStations_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }

        private void BtnPlants_Click(object sender, EventArgs e)
        {
            HideSubMenu();
        }
        #endregion Setting

       
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void lblUserName_Click(object sender, EventArgs e)
        {
            btnLogOut.Visible = true;
        }

        private void FrmHome_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            OpenChildForm(new AddUser(Global.LoginUserID, Global.UserRole));
        }

        private void BtnHome_Click(object sender, EventArgs e)
        {
            OpenChildForm(new AddUser(Global.LoginUserID, Global.UserRole));
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmLogin login = new FrmLogin();
            login.Show();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (pnlLogout.Visible ==true)
            {
                pnlLogout.Visible = false;
            }
            else if (pnlLogout.Visible == false)
            {
                pnlLogout.Visible = true;
                pnlLogout.BringToFront();

                /*
                PnlChildForm.SendToBack();
               BtnChangePassword.BringToFront();
                */

            }

        }

        private void PnlChildForm_Paint(object sender, PaintEventArgs e)
        {
            this.Hide();
            FrmLogin login = new FrmLogin();
            login.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmLogin login = new FrmLogin();
            login.Show();
            
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnLogOut_Click_1(object sender, EventArgs e)
        {

        }
    }
}
