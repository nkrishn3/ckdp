﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CKDPacking
{
    public partial class frmAssignRole : Form
    {
        int StationCount = 0;
        int sno = 0;

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public frmAssignRole()
        {
            InitializeComponent();
            FillCombobox();

            //            this.dtpFromDate.Value = DateTime.Now;
            this.dtpFromDate.Value = Convert.ToDateTime("01/01/1990");
            this.dtpToDate.Value = DateTime.Now;

             BindGrid(string.Empty, string.Empty, string.Empty, dtpFromDate.Value.ToString("MM/dd/yyyy"), dtpToDate.Value.ToString("MM/dd/yyyy"));
//            BindGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
        }
        #region formInitial
        protected void FillCombobox()

        {
            DataRow drPlant;
            DataRow drStation;
            DataRow drUser;
               try
            {
                SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                con.Open();
                SqlCommand cmd = new SqlCommand(ApplicationConstants.GET_PLANTS, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                drPlant = dt.NewRow();
                //dr.ItemArray = new object[] { 0, "--Select Role--" };
                dt.Rows.InsertAt(drPlant, 0);
                cmbPlant.ValueMember = "plantId";
                cmbPlant.DisplayMember = "plantName";
                cmbPlant.DataSource = dt;
                con.Close();
            }
            catch (SqlException ex)
            {
                log.Error("SqlException while getting plants:FillCombobox()-" + ex.Message);
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                log.Error("Exception while getting plants:FillCombobox()-" + ex.Message);
                MessageBox.Show(ex.Message);
            }
        
         try
            {
                SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                con.Open();
                SqlCommand cmd = new SqlCommand(ApplicationConstants.GET_STATIONS, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                drStation = dt.NewRow();
                //dr.ItemArray = new object[] { 0, "--Select Role--" };
                dt.Rows.InsertAt(drStation, 0);
               cmbStation.ValueMember = "stationId";
                cmbStation.DisplayMember = "stationNumber";
                cmbStation.DataSource = dt;
                con.Close();
            }
            catch (SqlException ex)
            {
                log.Error("SqlException While Getting Stations:FillCombobox()-" + ex.Message);
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                log.Error("Exception While Getting Stations:FillCombobox()-" + ex.Message);
                MessageBox.Show(ex.Message);
            }

       
            try
                {
                    SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                    con.Open();
                    SqlCommand cmd = new SqlCommand(ApplicationConstants.GET_USERLOV, con);
                    SqlDataAdapter sda = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                      drUser = dt.NewRow();
                    //dr.ItemArray = new object[] { 0, "--Select Role--" };
                    dt.Rows.InsertAt(drUser, 0);
                    cmbUser.ValueMember = "userid";
                    cmbUser.DisplayMember = "username";
                    cmbUser.DataSource = dt;
                    con.Close();
                }
                catch (SqlException ex)
                {
                    log.Error("SqlException while getting user Lov:BindUsers()-FillCombobox()-" + ex.Message);
                    MessageBox.Show(ex.Message);
                }
                catch (Exception ex)
                {
                    log.Error("Exception while getting user Lov:BindUsers()-FillCombobox()-" + ex.Message);
                    MessageBox.Show(ex.Message);
                }
         

       }
        private void BindGrid(string PlantName, string UserId, string StationNumber, string FromDate, string ToDate)
        {
            string f_date = FromDate;
            string t_Date = ToDate;
         
           


            try
            {
                SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                SqlCommand cmd = new SqlCommand(ApplicationConstants.GET_USERSTATIONS, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PlantName", PlantName);
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@StationNumber", StationNumber) ;
                cmd.Parameters.AddWithValue("@FromDate", f_date);
                cmd.Parameters.AddWithValue("@ToDate", ToDate);
                cmd.Parameters.AddWithValue("@UserStId",0);

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dgvUserStations.DataSource = dt;
                dgvUserStations.Columns[0].Visible = false;
                //DgvUser.Columns[09].DefaultCellStyle.Format = ApplicationConstants.DateFormat;
                //DgvUser.Columns[11].DefaultCellStyle.Format = ApplicationConstants.DateFormat;
                dgvUserStations.EditMode = DataGridViewEditMode.EditProgrammatically;
                con.Close();
            }
            catch (SqlException ex)
            {
                log.Error("SqlException getting User per Station:BindGrid()" + ex.Message);
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                log.Error("Exception while getting User per Stations:BindGrid()" + ex.Message);
                MessageBox.Show(ex.Message);
            }

        }
        private void ClearData()
        {
            cmbPlant.Text = "";
            cmbPlant.Text = "";
            cmbUser.Text = "";
            this.dtpFromDate.Value = DateTime.Now;
            this.dtpToDate.Value = DateTime.Now;
            StationCount = 0;
            sno = 0;
            btnAddUser.Enabled = true;
            btnSearch.Enabled = true;
            btnDelete.Enabled = true;
            btnUpdate.Enabled = true;


        }
        #endregion formInitial

        private bool ValidateData()
        {
            erpUserStation.Clear();
            bool Isvalid = true;
            if (string.IsNullOrEmpty(cmbPlant.Text))
            {
                cmbPlant.Focus();
                erpUserStation.SetError(cmbPlant, "Please select the Plant");
                Isvalid = false;
            }

            if (string.IsNullOrEmpty(cmbStation.Text))
            {
                cmbStation.Focus();
                erpUserStation.SetError(cmbStation, "Please select the Station");
                Isvalid = false;
            }
            
            if (string.IsNullOrEmpty(cmbUser.Text))
            { 
                cmbUser.Focus();
                erpUserStation.SetError(cmbUser, "Please select the User Name");
                Isvalid = false;
            }
            if (dtpFromDate.Value>dtpToDate.Value)
            {
                dtpFromDate.Focus();
                erpUserStation.SetError(cmbUser, "From Date Shold not be greater than End Date");
                Isvalid = false;
            }

            return Isvalid;
        }
        private int ValidateStationAssigment()
        {            
            
            string f_date = dtpFromDate.Value.ToString("MM/dd/yyyy");
            string t_Date = dtpToDate.Value.ToString("MM/dd/yyyy");
          try
            {
                SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                SqlCommand cmd = new SqlCommand(ApplicationConstants.GET_USERSTATIONS, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PlantName", cmbPlant.Text);
                cmd.Parameters.AddWithValue("@UserId", cmbUser.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@StationNumber", cmbStation.Text);
                cmd.Parameters.AddWithValue("@FromDate", f_date);
                cmd.Parameters.AddWithValue("@ToDate", t_Date);
                cmd.Parameters.AddWithValue("@UserStId", 0);
                SqlDataAdapter adapt = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adapt.Fill(ds);
                con.Close();
                 StationCount = ds.Tables[0].Rows.Count;
                log.Info("Record count:ValidateStationAssigment()" + StationCount);
                
              /* if (Result > 0)
                {
                    Isvalid = false;
                    log.Info("Station has already assgined to user:" + cmbUser.Text + "for from date "+ ds.Tables[0].Rows[0].ItemArray[4].ToString()+" "+"Todate "+ ds.Tables[0].Rows[0].ItemArray[5].ToString());
                                      
                }
                if (Result == 0)
                {
                    Isvalid = true;
                    log.Info("Station not assgined to for user for mentioned from date and todate");
                }
              */

              }
             
      
            catch (SqlException ex)
            {
                log.Error("SqlException getting User per Station:ValidateStationAssigment()" + ex.Message);
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                log.Error("Exception while getting User per Stations:ValidateStationAssigment()" + ex.Message);
                MessageBox.Show(ex.Message);
            }

            return StationCount;
        }
        private void btnSearch_Click(object sender, EventArgs e)
          {
           
            BindGrid(cmbPlant.Text,cmbUser.SelectedValue.ToString(),cmbStation.Text, dtpFromDate.Value.ToString("MM/dd/yyyy"), dtpToDate.Value.ToString("MM/dd/yyyy"));
            //log.Info("--------------SEARCH---------");
            //log.Info("User: " + cmbUser.Text + "\nStation " + cmbStation.Text + "\nfrom Date " + dtpFromDate.Value.ToString("MM/dd/yyyy") + "\nto Date " + dtpToDate.Value.ToString("MM/dd/yyyy"));
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            cmbPlant.Text = "";
            cmbStation.Text = "";
            cmbUser.Text = "";
            StationCount = 0;
            sno = 0;
            this.dtpFromDate.Value = DateTime.Now;
            this.dtpToDate.Value = DateTime.Now;
            btnAddUser.Enabled = true;
            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;
            btnSearch.Enabled = true;
            btnDelete.Enabled = true;
            BindGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
        }

        private void dtpFromDate_ValueChanged(object sender, EventArgs e)
        {

        }

        private void dtpFromDate_DropDown(object sender, EventArgs e)
        {
            dtpFromDate.Format = DateTimePickerFormat.Short;
            dtpFromDate.Value = DateTime.Today;
        }

        private void dtpToDate_DropDown(object sender, EventArgs e)
        {
           dtpToDate.Format = DateTimePickerFormat.Short;
            dtpToDate.Value = DateTime.Today;
        }

        private void btnAddUser_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                if (ValidateStationAssigment()==0)
                {
                    try
                    {
                        SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());

                        con.Open();

                        SqlCommand cmd = new SqlCommand(ApplicationConstants.INSERT_USERSTATIONS, con);
                        cmd.Parameters.AddWithValue("@PlantId", cmbPlant.SelectedValue.ToString());
                        cmd.Parameters.AddWithValue("@UserId", cmbUser.SelectedValue.ToString());
                        cmd.Parameters.AddWithValue("@StationId", cmbStation.SelectedValue.ToString());
                        cmd.Parameters.AddWithValue("@FromDate", dtpFromDate.Value.ToString("MM/dd/yyyy"));
                        cmd.Parameters.AddWithValue("@ToDate", dtpToDate.Value.ToString("MM/dd/yyyy"));
                        cmd.Parameters.AddWithValue("@CreatedBy", Global.LoginUserID);
                        cmd.CommandType = CommandType.StoredProcedure;
                        int result = cmd.ExecuteNonQuery();
                        con.Close();
                        if (result > 0)
                        {
                            MessageBox.Show("Station assigned to user successful!",
                               "Information",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Information,
                               MessageBoxDefaultButton.Button1);
                            log.Warn("Station Assigned to the user" + " " + cmbUser.SelectedText.ToString() + "\n Station " + cmbStation.Text + " \n from Date " + dtpFromDate.Text + "\n To date: " + dtpToDate.Text + " Combination assigned by " + " " + Global.LoginUserID);
                            BindGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                            ClearData();
                            
                        }
                    }
                    catch (SqlException ex)
                    {
                        log.Error("SqlException while assign station to user:" + ex.Message);
                        MessageBox.Show(ex.Message);
                    }
                    catch (Exception ex)
                    {
                        log.Debug("Error while assign station to user" + ex.Message);
                        log.Error("Error while assign station to user" + ex.Message);
                        MessageBox.Show(ex.Message);
                    }
                }
               
                else
                {
                    MessageBox.Show("Station already assigned to " + "\n"+cmbUser.Text + "\n Station " + cmbStation.Text + "\n from date- " + dtpFromDate.Text + "\n to date " + dtpToDate.Text + "Combination",
                              "Error",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Error,
                              MessageBoxDefaultButton.Button1);
                }

            }
            else
            {
                MessageBox.Show($"Please correct validation errors:\n {erpUserStation.GetError(cmbUser)} \n { erpUserStation.GetError(cmbStation)} \n { erpUserStation.GetError(cmbPlant)}\n {erpUserStation.GetError(dtpFromDate)}",
             "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

         }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {

                if (ValidateStationAssigment()==0)
                {
                    if (sno != 0)
                    {
                        log.Info("Mentioned User Station ID: " + sno + "Update Initiaate by" + Global.LoginUserID);
                        try
                        {
                            SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());

                            con.Open();

                            SqlCommand cmd = new SqlCommand(ApplicationConstants.UPDATE_USERSTATIONS, con);
                            cmd.Parameters.AddWithValue("@PlantId", cmbPlant.SelectedValue.ToString());
                            cmd.Parameters.AddWithValue("@UserId", cmbUser.SelectedValue.ToString());
                            cmd.Parameters.AddWithValue("@StationId", cmbStation.SelectedValue.ToString());
                            cmd.Parameters.AddWithValue("@FromDate", dtpFromDate.Value.ToString("MM/dd/yyyy"));
                            cmd.Parameters.AddWithValue("@ToDate", dtpToDate.Value.ToString("MM/dd/yyyy"));
                            cmd.Parameters.AddWithValue("@CreatedBy", Global.LoginUserID);
                            cmd.Parameters.AddWithValue("@UserStId", sno);



                            cmd.CommandType = CommandType.StoredProcedure;
                            int result = cmd.ExecuteNonQuery();
                            con.Close();
                            if (result > 0)
                            {
                                MessageBox.Show("Station assignment to user successful!",
                                   "Information",
                                   MessageBoxButtons.OK,
                                   MessageBoxIcon.Information,
                                   MessageBoxDefaultButton.Button1);
                                log.Info("Station Assigned to the user" + " " + cmbUser.SelectedText.ToString() + " " + "Added by" + " " + Global.LoginUserID);
                                BindGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                                ClearData();
                                
                            }
                        }
                        catch (SqlException ex)
                        {
                            
                            log.Error("SqlException while update station assignment to user:" + ex.Message);
                            MessageBox.Show(ex.Message);
                        }
                        catch (Exception ex)
                        {
                            log.Debug("Error while update station assignment" + ex.Message);
                            log.Error("Error while update station assignment" + ex.Message);
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Station already assigned to selected period for the user " + cmbUser.Text,
                              "Error",
                              MessageBoxButtons.OK,
                              MessageBoxIcon.Error,
                              MessageBoxDefaultButton.Button1);
                    log.Error("Station already assigned to selected period for the user " + cmbUser.Text);
                }
            }

            else
            {
                MessageBox.Show($"Please correct validation errors:\n {erpUserStation.GetError(cmbUser)} \n { erpUserStation.GetError(cmbStation)} \n { erpUserStation.GetError(cmbPlant)}\n ",
             "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                if (ValidateStationAssigment()!=0)
                {
                    DialogResult dialogResult = MessageBox.Show("Do you want to delete this Station Assignment", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        log.Info("User Station Id " + sno + "Delete Initiated by" + Global.LoginUserID);
                        try
                        {
                            SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                            SqlCommand cmd;
                            con.Open();

                            cmd = new SqlCommand(ApplicationConstants.DELETE_USERSTATIONS, con);
                            cmd.Parameters.AddWithValue("@UserStId", sno);
                            cmd.CommandType = CommandType.StoredProcedure;
                            int result = cmd.ExecuteNonQuery();
                            con.Close();
                            if (result > 0)
                            {
                                MessageBox.Show(
                                                "Station assignment deleted successful!",
                                                "Information",
                                                MessageBoxButtons.OK,
                                                MessageBoxIcon.Information,
                                                MessageBoxDefaultButton.Button1
                                            );
                                
                                log.Info("User: " + cmbUser.Text + " Station " + cmbStation.Text + " from Date " + dtpFromDate.Text + " to Date " + dtpToDate.Text + " Combination Deleted by" + " " + Global.LoginUserID);
                                BindGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                                ClearData();
                                // btnUpdate.Visible = false;
                                // btnDelete.Visible = false;
                                // btnAddUser.Visible = true;

                            }
                        }
                        catch (SqlException ex)
                        {
                            log.Error("SqlException while delete station to user:" + ex.Message);
                            MessageBox.Show(ex.Message);
                        }
                        catch (Exception ex)
                        {
                            log.Debug("Error while delete the station to user:" + ex.Message);
                            log.Error("Error while delete the to user:" + ex.Message);
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Station not assigned to seleted period for the user " + cmbUser.Text,
                             "Error",
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Error,
                             MessageBoxDefaultButton.Button1);
                    log.Error("Station not assigned to seleted period for the user " + cmbUser.Text);
                }

            }
            else
            {
                MessageBox.Show($"Please correct validation errors:\n {erpUserStation.GetError(cmbUser)} \n { erpUserStation.GetError(cmbStation)} \n { erpUserStation.GetError(cmbPlant)}\n ",
             "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvUserStations_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            erpUserStation.Clear();

            if (e.RowIndex > -1)
            {

                sno = Convert.ToInt32(dgvUserStations.Rows[e.RowIndex].Cells[0].Value.ToString());
                cmbPlant.Text = dgvUserStations.Rows[e.RowIndex].Cells[1].Value.ToString();
                cmbStation.Text = dgvUserStations.Rows[e.RowIndex].Cells[5].Value.ToString();
                cmbUser.Text = dgvUserStations.Rows[e.RowIndex].Cells[4].Value.ToString() + " (" + dgvUserStations.Rows[e.RowIndex].Cells[3].Value.ToString() + "-" + dgvUserStations.Rows[e.RowIndex].Cells[2].Value.ToString() + ")";
                dtpFromDate.Value = Convert.ToDateTime(dgvUserStations.Rows[e.RowIndex].Cells[6].Value.ToString());
                dtpToDate.Value = Convert.ToDateTime(dgvUserStations.Rows[e.RowIndex].Cells[7].Value.ToString());

                /*log.Info("--------------DOUBLE---------");
                log.Info("User: " + cmbUser.Text + "\nStation " + cmbStation.Text + "\nfrom Date " + Convert.ToDateTime(dgvUserStations.Rows[e.RowIndex].Cells[6].Value.ToString()) + "\nto Date " + Convert.ToDateTime(dgvUserStations.Rows[e.RowIndex].Cells[7].Value.ToString()));
                log.Info("sno" + sno);
                */
                btnUpdate.Enabled = true;
                btnAddUser.Enabled = false;
                btnSearch.Enabled = false;
                
                


            }
        }

        private void frmAssignRole_Load(object sender, EventArgs e)
        {

        }
    }
}
