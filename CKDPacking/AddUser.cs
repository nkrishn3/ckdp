﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CKDPacking
{
    public partial class AddUser : Form
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        int sno = 0;
        string create_emp_id;
        string create_user_type;
        public AddUser(string empid, string usertype)
        {
            InitializeComponent();
            create_emp_id = empid;
            create_user_type = usertype;
            BindRole();
            BindGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
        }

        private void BtnAddUser_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {

                try
                {
                    SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());

                    con.Open();

                    SqlCommand cmd = new SqlCommand(ApplicationConstants.USER_INSERT, con);
                    cmd.Parameters.AddWithValue("@EmployeeId",txtEmployeeID.Text);
                    cmd.Parameters.AddWithValue("@EmployeeName", txtEmployeeName.Text);
                    cmd.Parameters.AddWithValue("@ContratorName", txtContractor.Text);
                    cmd.Parameters.AddWithValue("@UserName", txtUserName.Text);
                    cmd.Parameters.AddWithValue("@Password", txtPassword.Text);
                    cmd.Parameters.AddWithValue("@Role", cmbRole.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", create_emp_id);
                    cmd.CommandType = CommandType.StoredProcedure;
                    int result = cmd.ExecuteNonQuery();
                    con.Close();
                    if (result > 0)
                    {
                        MessageBox.Show("User Created Successful!",
       "Information",
       MessageBoxButtons.OK,
       MessageBoxIcon.Information,
       MessageBoxDefaultButton.Button1);
                        log.Warn("User " + " " + txtUserName.Text + " " + "created by" + " " + create_emp_id);
                        BindGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                        ClearData();
                        
                    }
                }
                catch (SqlException ex)
                {
                    if (ex.Number == 2601)
                    {
                        log.Debug("User Details you have entered already exists" + ex.Message);
                        MessageBox.Show("User Details already exists", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error,
                            MessageBoxDefaultButton.Button1);
                    }
                    log.Error("SqlException While Create User BtnAddUser_Click()-"+ex.Message);
                    MessageBox.Show(ex.Message);
                }
                catch (Exception ex)
                {
                    log.Debug("Error While Inserting User Master" + ex.Message);
                    log.Error("Error While Inserting User Master" + ex.Message);
                    MessageBox.Show(ex.Message);
                }

            }
          else
            {

                MessageBox.Show($"Please Enter all the details",
                                 "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

       
        private void BindGrid(string EmployeeId, string EmployeeName, string EmployeeUserName, string ContractorName, string EmployeeRole)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                SqlCommand cmd = new SqlCommand(ApplicationConstants.USER_SELECT_QUERY, con);
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@EmployeeId", EmployeeId);
                cmd.Parameters.AddWithValue("@EmployeeName", EmployeeName);
                cmd.Parameters.AddWithValue("@EmployeeUserName", EmployeeUserName);
                cmd.Parameters.AddWithValue("@ContractorName", ContractorName);
                cmd.Parameters.AddWithValue("@EmployeeRole", EmployeeRole);
                cmd.Parameters.AddWithValue("@UserId", 0);

                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                DgvUser.DataSource = dt;
                DgvUser.Columns[0].Visible = false;
                DgvUser.Columns[2].Visible = false;
                //DgvUser.Columns[09].DefaultCellStyle.Format = ApplicationConstants.DateFormat;
                //DgvUser.Columns[11].DefaultCellStyle.Format = ApplicationConstants.DateFormat;
                DgvUser.EditMode = DataGridViewEditMode.EditProgrammatically;
                con.Close();
            }
            catch (SqlException ex)
            {
                log.Info("SqlException While Getting Role" + ex.Message);
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                log.Info("Exception While Getting Role" + ex.Message);
                MessageBox.Show(ex.Message);
            }

        }
        public void BindRole()
        {
            DataRow dr;

            try
            {
                SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                con.Open();
                SqlCommand cmd = new SqlCommand(ApplicationConstants.GET_ROLES, con);
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                dr = dt.NewRow();
                //dr.ItemArray = new object[] { 0, "--Select Role--" };
                dt.Rows.InsertAt(dr, 0);
                cmbRole.ValueMember = "roleId";
                cmbRole.DisplayMember = "roleName";
                cmbRole.DataSource = dt;
                con.Close();
            }
            catch (SqlException ex)
            {
                log.Info("SqlException While Getting Role:" + ex.Message);
                MessageBox.Show(ex.Message);
            }
            catch (Exception ex)
            {
                log.Info("Exception While Getting Role:" + ex.Message);
                MessageBox.Show(ex.Message);
            }
        }
        private bool ValidateData()
        {
            erpUserPage.Clear();
            bool Isvalid = true;

            if (string.IsNullOrEmpty(txtEmployeeID.Text))
            {
                txtEmployeeID.Focus();
                erpUserPage.SetError(txtEmployeeID, "Please Enter Employee Id");
                Isvalid = false;
            }

            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                txtPassword.Focus();
                erpUserPage.SetError(txtPassword, "Please Enter Password");
                Isvalid = false;
            }

           if (string.IsNullOrEmpty(txtEmployeeName.Text.Trim()))
            {
                txtEmployeeName.Focus();
                erpUserPage.SetError(txtEmployeeName, "Please Enter Employee Full Name");
                Isvalid = false;
            }


            if (string.IsNullOrEmpty(txtContractor.Text.Trim()))
            {
                txtContractor.Focus();
                erpUserPage.SetError(txtContractor, "Please Enter Contratore Name");
                Isvalid = false;
            }

            if (string.IsNullOrEmpty(txtUserName.Text.Trim()))
            {
                txtUserName.Focus();
                erpUserPage.SetError(txtUserName, "Please Enter User Name ");
                Isvalid = false;
            }


            if (string.IsNullOrEmpty(cmbRole.Text.Trim()))
            {
                cmbRole.Focus();
                erpUserPage.SetError(cmbRole, "Please Select the Role");
                Isvalid = false;
            }


            return Isvalid;
        }
        private void ClearData()
        {
            txtEmployeeID.Text = "";
            sno = 0;
            txtEmployeeName.Text = "";

            txtContractor.Text = "";
            txtUserName.Text = "";
            txtPassword.Text = "";
            cmbRole.Text = "";
            txtEmployeeID.ReadOnly = false;
            txtUserName.ReadOnly = false;
           
        }
        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            BindGrid(txtEmployeeID.Text,txtEmployeeName.Text, txtUserName.Text, txtContractor.Text, cmbRole.Text);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void DgvUser_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
           
                
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (ValidateData())
            {
                
                try
                {                                 
                    SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                    SqlCommand cmd;
                    con.Open();

                    cmd = new SqlCommand(ApplicationConstants.USER_UPDATE, con);
                    cmd.Parameters.AddWithValue("@EmployeeId", txtEmployeeID.Text);
                    cmd.Parameters.AddWithValue("@UserName", txtUserName.Text);
                    cmd.Parameters.AddWithValue("@Password", txtPassword.Text);
                    cmd.Parameters.AddWithValue("@EmployeeName", txtEmployeeName.Text);
                    cmd.Parameters.AddWithValue("@ContractorName", txtContractor.Text);
                    cmd.Parameters.AddWithValue("@Role", cmbRole.Text);
                    cmd.Parameters.AddWithValue("@CreatedBy", create_emp_id);
                    cmd.CommandType = CommandType.StoredProcedure;

                    int result = cmd.ExecuteNonQuery();
                    con.Close();
                    if (result > 0)
                    {
                        log.Warn("User Detail" + " " + txtUserName.Text + " " + "Updated by" + " " + create_emp_id);
                        MessageBox.Show("User Updated Successful!",
       "Information",
       MessageBoxButtons.OK,
       MessageBoxIcon.Information,
       MessageBoxDefaultButton.Button1);
                        BindGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                        ClearData();
                       // btnUpdate.Visible = false;
                        //btnDelete.Visible = false;
                       // btnAddUser.Visible = true;
                  

                        
                    }
                }
                catch (Exception ex)
                {
                    log.Debug("User Master cannot be updated" + ex.Message);
                    log.Error("Error While Updating User Master" + ex.Message);
                    MessageBox.Show(ex.Message);
                }
            }


            else
            {
                MessageBox.Show($"Please correct validation errors:\n {erpUserPage.GetError(txtUserName)} \n { erpUserPage.GetError(txtPassword)} \n { erpUserPage.GetError(txtEmployeeName)}\n { erpUserPage.GetError(txtContractor)} \n { erpUserPage.GetError(txtEmployeeID)} \n ",
             "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //if (ValidateData())
           // {

                if (sno != 0)
                {
                    DialogResult dialogResult = MessageBox.Show("Do you want to delete this user", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (dialogResult == DialogResult.Yes)
                    {
                        try
                        {

                            SqlConnection con = new SqlConnection(ConnectionString.GetConnectionString());
                            SqlCommand cmd;
                            con.Open();

                            cmd = new SqlCommand(ApplicationConstants.USER_DELETE, con);
                            cmd.Parameters.AddWithValue("@UserId", sno);
                            cmd.CommandType = CommandType.StoredProcedure;
                            int result = cmd.ExecuteNonQuery();
                            con.Close();
                            if (result > 0)
                            {
                                MessageBox.Show(
                                                "User Deleted Successful!",
                                                "Information",
                                                MessageBoxButtons.OK,
                                                MessageBoxIcon.Information,
                                                MessageBoxDefaultButton.Button1
                                            );
                                log.Warn("User Name:" + " " + txtUserName.Text + " " + "Deleted by" + " " + create_emp_id);
                                BindGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
                                ClearData();
                                // btnUpdate.Visible = false;
                                // btnDelete.Visible = false;
                                // btnAddUser.Visible = true;

                            }
                        }
                        catch (SqlException ex)
                        {
                            /* if (ex.Number == 547)
                             {
                                 log.Debug("The user Master is already in use in another transaction" + ex.Message);
                                 MessageBox.Show("User could not be deleted it is already referenced in some other transactions", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error,
                                     MessageBoxDefaultButton.Button1);
                             }
                            */

                            log.Info(ex.Message);
                            MessageBox.Show(ex.Message);

                        }
                        catch (Exception ex)
                        {
                            log.Debug("THe User Master Cannot be deleted" + ex.Message);
                            log.Error("Error While Deleting User Master" + ex.Message);
                            MessageBox.Show(ex.Message);
                        }
                    }

                    /*else if (dialogResult == DialogResult.No)
                    {
                            ClearData();
                    }
                    */

                }
                else
                {
                    MessageBox.Show("Please doble click on user to delete");
                }
                
                                    
             
              /* else
              }
             {
                   MessageBox.Show($"Please Select User Details:\n {erpUserPage.GetError(txtUserName)} \n { erpUserPage.GetError(txtPassword)} \n { erpUserPage.GetError(txtEmployeeName)}\n { erpUserPage.GetError(txtContractor)} \n { erpUserPage.GetError(txtEmployeeID)} \n ",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
             }
             */
            
        }

        private void DgvUser_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            erpUserPage.Clear();

            if (e.RowIndex > -1)
            {

                sno = Convert.ToInt32(DgvUser.Rows[e.RowIndex].Cells[0].Value.ToString());
                txtEmployeeID.Text = DgvUser.Rows[e.RowIndex].Cells[1].Value.ToString();
                txtPassword.Text = DgvUser.Rows[e.RowIndex].Cells[2].Value.ToString();
                txtUserName.Text = DgvUser.Rows[e.RowIndex].Cells[3].Value.ToString();
                txtEmployeeName.Text = DgvUser.Rows[e.RowIndex].Cells[4].Value.ToString();
                txtContractor.Text = DgvUser.Rows[e.RowIndex].Cells[5].Value.ToString();
                cmbRole.Text = DgvUser.Rows[e.RowIndex].Cells[8].Value.ToString();
                txtEmployeeID.ReadOnly = true;
                txtUserName.ReadOnly = true;
                btnUpdate.Visible = true;
                btnAddUser.Enabled = false;
                btnSearch.Enabled = false;
                
               
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearData();
            txtEmployeeID.Text = "";
            sno = 0;
            txtEmployeeName.Text = "";

            txtContractor.Text = "";
            txtUserName.Text = "";
            txtPassword.Text = "";
            cmbRole.Text = "";
            txtEmployeeID.ReadOnly = false;
            txtUserName.ReadOnly = false;
            btnAddUser.Enabled = true;
            btnUpdate.Enabled = true;
            btnDelete.Enabled = true;
            btnSearch.Enabled = true;
            btnDelete.Enabled = true;

        }

        private void cmbRole_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void DgvUser_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}

